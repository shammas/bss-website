/**
 * Created on 18/11/2020.
 */


app.controller('CenterController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.centers = [];
        $scope.newcenter = {};
        $scope.curcenter = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadCenter();

        function loadCenter() {
            $http.get($rootScope.base_url + 'dashboard/center/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.centers = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newCenter = function () {
            $scope.newcenter = {};
            $scope.filespre = [];
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            
        };

        $scope.editCenter = function (item) {
            $scope.showform = true;
            $scope.curcenter = item;
            $scope.newcenter = angular.copy(item);
             };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addCenter = function () {

            var fd = new FormData();

            angular.forEach($scope.newcenter, function (item, key) {
                fd.append(key, item);
            });

            if ($scope.newcenter['id']) {
                var url = $rootScope.base_url + 'dashboard/center/edit/' + $scope.newcenter.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadCenter();
                        $scope.newcenter = {};
                        $scope.showform = false;
                        $scope.files = '';
                        $scope.validationError = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
               
                    var url = $rootScope.base_url + 'dashboard/center/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadCenter();
                            $scope.newcenter = {};
                            $scope.showform = false;
                            $scope.files = false;
                            $scope.validationError = '';

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
            }
        };

        $scope.deleteCenter = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/center/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.centers.indexOf(item);
                                    $scope.centers.splice(index, 1);
                                    loadCenter();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

 /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (center, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return center;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);

