<?php
/**
 * 002_initial_schema.php
 * Date: 06/03/19
 * Time: 02:35 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

    public function up()
    {

        /**
         * Table structure for table 'galleries'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('galleries');

        /**
         * Table structure for table 'ci_sessions'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => 128,
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => 45,
            ],
            'timestamp' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'data' => [
                'type' => 'BLOB',
            ]
        ]);
        $this->dbforge->add_key(['id', 'ip_address'], TRUE);
        $this->dbforge->create_table('ci_sessions');
        $this->db->query("ALTER TABLE `ci_sessions` ADD KEY `ci_sessions_timestamp` (`timestamp`)");

    }

    public function down()
    {
        $this->dbforge->drop_table('galleries', TRUE);
    }
}