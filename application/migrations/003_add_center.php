<?php
/**
 * 003_add_center.php
 * Date: 06/03/19
 * Time: 02:35 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_center extends CI_Migration {

    public function up()
    {

        /**
         * Table structure for table 'centers'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('centers');

        

    }

    public function down()
    {
        $this->dbforge->drop_table('centers', TRUE);
    }
}