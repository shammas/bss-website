<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>BSS | Login</title>
    <link href="icon/favicon.png" rel="shortcut icon">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/bootstrap.min.css" rel="stylesheet">
    
    <link href="<?php echo base_url(); ?>./assets/dashboard/css/style.css" rel="stylesheet">
    

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <!-- <h1 class="logo-name">MUBARAK</h1> -->
                 <!-- <img src="img/logo.png"> -->
            </div>
            <h3>Welcome to BSS</h3>
            <p>Perfectly designed and precisely prepared admin pages with extra new web app views.
            </p>
            <p>Login in. To see it in action.</p>
              <div id="infoMessage"><?php echo $message;?></div>
              
                <?php echo form_open(base_url('login'), ['class' => 'm-t', 'role' => 'form']);?>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" required="" name="identity" id="identity">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password" id="password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" >Login</button>

                <a href="auth/forgot_password"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>
            </form>
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
        </div>
    </div>

<!-- Mainly scripts -->
<!-- <script src="<?php echo asset() . 'dashboard/';?>js/jquery-3.1.1.min.js"></script> -->
<!-- <script src="<?php echo asset() . 'dashboard/';?>js/bootstrap.min.js"></script> -->

<script src="<?php echo base_url(); ?>./assets/dashboard/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>./assets/dashboard/js/bootstrap.min.js"></script>

</body>

</html>
