
    <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>Special Courses</h1>
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
       <div class="container content">
            <hr class="space m" />
              <h3 class=" text-center text-color">SPECIAL COURSES #1: AVIATION COURSES</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                    <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>AS001</td>
                        <td>BSS DIPLOMA IN INTERNATIONAL AIRLINE & TRAVEL MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>AS002</td>
                        <td>BSS DIPLOMA IN AIRPORT MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>AS003</td>
                        <td>BSS DIPLOMA IN CABIN CREW MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>AS004</td>
                        <td>BSS DIPLOMA IN AIR CARGO PRACTICES & DOCUMENTATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>5</td>
                        <td>AS005</td>
                        <td>BSS DIPLOMA IN DOMESTIC AIRLINE & TRAVEL MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>AS006</td>
                        <td>BSS DIPLOMA IN AIRLINE TRAVEL & TOURISM AND HOSPITALITY MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>7</td>
                        <td>AS007</td>
                        <td>AIR HOSTESS TRAINING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>AS008</td>
                        <td>BSS DIPLOMA IN AIRPORT GROUND STAFF TRAINING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>9</td>
                        <td>AS009</td>
                        <td>CERTIFICATE IN AIR FARES & TICKETING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>AS010</td>
                        <td>BSS DIPLOMA IN AIR FARE & TICKETING MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>11</td>
                        <td>AS011</td>
                        <td>BSS DIPLOMA IN AVIATION SAFETY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>AS012</td>
                        <td>BSS DIPLOMA IN AIRLINE MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>AS013</td>
                        <td>BSS DIPLOMA IN CARGO & HOTEL MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>AS014</td>
                        <td>BSS DIPLOMA IN AIRPORT & HOTEL MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>15</td>
                        <td>AS015</td>
                        <td>BSS DIPLOMA IN AVIATION & HOTEL MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>AS016</td>
                        <td>BSS DIPLOMA IN AIR/SEA CARGO MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>17</td>
                        <td>AS017</td>
                        <td>BSS POST DIPLOMA IN AIRPORT MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>AS018</td>
                        <td>BSS DIPLOMA IN AVIATION AND HOSPITALITY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    
                </tbody>
            </table>
                  <hr class="space m" />
              <h3 class=" text-center text-color">SPECIAL COURSES #2: BIO-TECHNOLOGY COURSES</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                    <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>BTS001</td>
                        <td>BSS DIPLOMA IN BIO TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>BTS002</td>
                        <td>BSS DIPLOMA IN BIO INFORMATICS</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>BTS003</td>
                        <td>BSS DIPLOMA IN MICROBIOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>BTS004</td>
                        <td>BIO-TECH TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>5</td>
                        <td>BTS005</td>
                        <td>BSS DIPLOMA IN BIO-TECH LAB TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>BTS006</td>
                        <td>CERTIFICATE IN BIO INFORMATICS</td>
                          <td>ONE YEAR</td>
                    </tr>
                </tbody>
            </table>
                       <hr class="space m" />
              <h3 class=" text-center text-color">SPECIAL COURSES #3: BUSINESS COURSES</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                    <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>BUS001</td>
                        <td>BSS ADVANCED DIPLOMA IN BANKING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>BUS002</td>
                        <td>BSS ADVANCED DIPLOMA IN INVESTMENT BANKING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>BUS003</td>
                        <td>BSS ADVANCED DIPLOMA IN TREASURY AND FOREX MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>BUS004</td>
                        <td>BSS ADVANCED DIPLOMA IN INSURANCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>5</td>
                        <td>BUS005</td>
                        <td>BSS ADVANCED DIPLOMA IN ACCOUNTING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>BUS006</td>
                        <td>BSS ADVANCED DIPLOMA IN ACCOUNTING STANDARDS AND US GAAP</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>7</td>
                        <td>BUS007</td>
                        <td>BSS ADVANCED DIPLOMA IN FORENSIC ACCOUNTING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>BUS008</td>
                        <td>BSS ADVANCED DIPLOMA IN INFORMATION SYSTEMS AUDIT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                     
                        <td>9</td>
                        <td>BUS009</td>
                        <td>BSS ADVANCED DIPLOMA IN BUSINESS FINANCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>BUS010</td>
                        <td>BSS ADVANCED DIPLOMA IN FINANCIAL ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>11</td>
                        <td>BUS011</td>
                        <td>BSS ADVANCED DIPLOMA IN STRATEGIC FINANCE AND CONTROL</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>BUS012</td>
                        <td>BSS ADVANCED DIPLOMA IN INVESTMENT AND TAX PLANNING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>BUS013</td>
                        <td>BSS ADVANCED DIPLOMA IN CYBER LAW</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>BUS014</td>
                        <td>BSS ADVANCED DIPLOMA IN INTELLECTUAL PROPERTY RIGHTS</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>15</td>
                        <td>BUS015</td>
                        <td>BSS ADVANCED DIPLOMA IN ALTERNATIVE DISPUTE RESOLUTION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>BUS016</td>
                        <td>BSS ADVANCED DIPLOMA IN ALTERNATIVE DISPUTE RESOLUTION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>17</td>
                        <td>BUS017</td>
                        <td>BSS ADVANCED DIPLOMA IN INTERNATIONAL BUSINESS LAW</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>BUS018</td>
                        <td>BSS ADVANCED DIPLOMA IN SECURITIES LAW</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                     
                        <td>19</td>
                        <td>BUS019</td>
                        <td>BSS ADVANCED DIPLOMA IN BANKING LAWS & LOAN MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>BUS020</td>
                        <td>BSS ADVANCED DIPLOMA IN HOSPITAL LAWS & MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>21</td>
                        <td>BUS021</td>
                        <td>BSS ADVANCED DIPLOMA IN PROJECT MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>BUS022</td>
                        <td>BSS ADVANCED DIPLOMA IN EXPORT AND IMPORT MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>BUS023</td>
                        <td>BSS ADVANCED DIPLOMA IN MANAGEMENT OF E-BUSINESS</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>BUS024</td>
                        <td>BSS ADVANCED DIPLOMA IN GLOBAL STRATEGIC MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>25</td>
                        <td>BUS025</td>
                        <td>BSS ADVANCED DIPLOMA IN ENTERPRISE RISK MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>BUS026</td>
                        <td>BSS ADVANCED DIPLOMA IN MARKETING MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>27</td>
                        <td>BUS027</td>
                        <td>BSS ADVANCED DIPLOMA IN CUSTOMER RELATIONSHIP MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>BUS028</td>
                        <td>BSS ADVANCED DIPLOMA IN MARKETING FINANCIAL PRODUCTS</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                     
                        <td>29</td>
                        <td>BUS029</td>
                        <td>BSS ADVANCED DIPLOMA IN BRAND MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>BUS030</td>
                        <td>BSS ADVANCED DIPLOMA IN RETAIL MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>31</td>
                        <td>BUS031</td>
                        <td>BSS ADVANCED DIPLOMA IN MARKETING COMMUNICATIONS</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>BUS032</td>
                        <td>BSS ADVANCED DIPLOMA IN INDUSTRIAL MARKETING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>BUS033</td>
                        <td>BSS ADVANCED DIPLOMA IN HUMAN RESOURCE MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>BUS034</td>
                        <td>BSS ADVANCED DIPLOMA IN LEADERSHIP AND CHANGE MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>35</td>
                        <td>BUS035</td>
                        <td>BSS ADVANCED DIPLOMA IN TRAINING AND DEVELOPMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>BUS036</td>
                        <td>BSS ADVANCED DIPLOMA IN PERFORMANCE MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>37</td>
                        <td>BUS037</td>
                        <td>BSS ADVANCED DIPLOMA IN SUPPLY CHAIN MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>BUS038</td>
                        <td>BSS ADVANCED DIPLOMA IN QUALITY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                     
                        <td>39</td>
                        <td>BUS039</td>
                        <td>BSS DIPLOMA IN FINANCIAL ACCOUNTING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>BUS040</td>
                        <td>BSS DIPLOMA IN FINANCIAL STATEMENT ANALYSIS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                        
                        <td>41</td>
                        <td>BUS041</td>
                        <td>BSS DIPLOMA IN MANAGEMENT ACCOUNTING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>BUS042</td>
                        <td>BSS DIPLOMA IN AUDITING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>BUS043</td>
                        <td>BSS DIPLOMA IN FINANCIAL MARKETS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>BUS044</td>
                        <td>BSS DIPLOMA IN INTERNATIONAL FINANCE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>45</td>
                        <td>BUS045</td>
                        <td>BSS DIPLOMA IN MERGERS AND ACQUISITIONS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>BUS046</td>
                        <td>BSS DIPLOMA IN FINANCIAL INSTITUTIONS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>47</td>
                        <td>BUS047</td>
                        <td>BSS DIPLOMA IN FINANCIAL MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>BUS048</td>
                        <td>BSS DIPLOMA IN FINANCIAL SERVICES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>49</td>
                        <td>BUS049</td>
                        <td>BSS DIPLOMA IN FINANCIAL RISK MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>50</td>
                        <td>BUS050</td>
                        <td>BSS DIPLOMA IN EQUITY VALUATION</td>
                       <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>51</td>
                        <td>BUS051</td>
                        <td>BSS DIPLOMA IN DERIVATIVE VALUATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>52</td>
                        <td>BUS042</td>
                        <td>BSS DIPLOMA IN DEBT VALUATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>53</td>
                        <td>BUS053</td>
                        <td>BSS DIPLOMA IN SECURITY ANALYSIS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>54</td>
                        <td>BUS054</td>
                        <td>BSS DIPLOMA IN MUTUAL FUNDS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>55</td>
                        <td>BUS055</td>
                        <td>BSS DIPLOMA IN PORTFOLIO MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>56</td>
                        <td>BUS056</td>
                        <td>BSS DIPLOMA IN FINANCIAL PLANNING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>57</td>
                        <td>BUS057</td>
                        <td>BSS DIPLOMA IN MONEY & BANKING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>58</td>
                        <td>BUS058</td>
                        <td>BSS DIPLOMA IN COMMERCIAL BANKING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>59</td>
                        <td>BUS059</td>
                        <td>BSS DIPLOMA IN INTERNATIONAL BANKING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>60</td>
                        <td>BUS060</td>
                        <td>BSS DIPLOMA IN BANKING OPERATIONS</td>
                       <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>61</td>
                        <td>BUS061</td>
                        <td>BSS DIPLOMA IN CENTRAL BANKING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>62</td>
                        <td>BUS062</td>
                        <td>BSS DIPLOMA IN CREDIT MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>63</td>
                        <td>BUS063</td>
                        <td>BSS DIPLOMA IN BANK MARKETING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>64</td>
                        <td>BUS064</td>
                        <td>BSS DIPLOMA IN LIFE INSURANCE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>65</td>
                        <td>BUS065</td>
                        <td>BSS DIPLOMA IN HEALTH INSURANCE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>66</td>
                        <td>BUS066</td>
                        <td>BSS DIPLOMA IN INSURANCE UNDERWRITING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>67</td>
                        <td>BUS067</td>
                        <td>BSS DIPLOMA IN INSURANCE OPERATIONS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>68</td>
                        <td>BUS068</td>
                        <td>BSS DIPLOMA IN ACTUARIAL PRINCIPLES & PRACTICES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>69</td>
                        <td>BUS069</td>
                        <td>BSS DIPLOMA IN GENERAL INSURANCE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>                      
                        <td>70</td>
                        <td>BUS070</td>
                        <td>BSS DIPLOMA IN INSURANCE MARKETING</td>
                       <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>71</td>
                        <td>BUS071</td>
                        <td>BSS DIPLOMA IN CLAIMS MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>72</td>
                        <td>BUS072</td>
                        <td>BSS DIPLOMA IN INSURANCE INVESTMENTS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>73</td>
                        <td>BUS073</td>
                        <td>BSS DIPLOMA IN REINSURANCE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>74</td>
                        <td>BUS074</td>
                        <td>BSS DIPLOMA IN BUSINESS LAW</td>
                        <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>75</td>
                        <td>BUS075</td>
                        <td>BSS DIPLOMA IN INDIRECT TAXATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>76</td>
                        <td>BUS076</td>
                        <td>BSS DIPLOMA IN DIRECT TAXATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>77</td>
                        <td>BUS077</td>
                        <td>BSS DIPLOMA IN HOSPITAL LAWS & MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>78</td>
                        <td>BUS078</td>
                        <td>BSS DIPLOMA IN BANKING LAWS & LOAN MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>79</td>
                        <td>BUS079</td>
                        <td>BSS DIPLOMA IN BUSINESS STRATEGY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>80</td>
                        <td>BUS080</td>
                        <td>BSS DIPLOMA IN BUSINESS RESEARCH METHODS</td>
                       <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>81</td>
                        <td>BUS081</td>
                        <td>BSS DIPLOMA IN OPERATIONS MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>82</td>
                        <td>BUS082</td>
                        <td>BSS DIPLOMA IN MANAGEMENT CONTROL SYSTEMS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>83</td>
                        <td>BUS083</td>
                        <td>BSS DIPLOMA IN BUSINESS ECONOMICS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>84</td>
                        <td>BUS084</td>
                        <td>BSS DIPLOMA IN BUSINESS COMMUNICATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>85</td>
                        <td>BUS085</td>
                        <td>BSS DIPLOMA IN IT IN MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>86</td>
                        <td>BUS086</td>
                        <td>BSS DIPLOMA IN BUSINESS ETHICS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>87</td>
                        <td>BUS087</td>
                        <td>BSS DIPLOMA IN SALES & DISTRIBUTION MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>88</td>
                        <td>BUS088</td>
                        <td>BSS DIPLOMA IN SERVICES MARKETING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>89</td>
                        <td>BUS089</td>
                        <td>BSS DIPLOMA IN STRATEGIC MARKETING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>                      
                        <td>90</td>
                        <td>BUS090</td>
                        <td>BSS DIPLOMA IN MARKETING COMMUNICATIONS</td>
                       <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>91</td>
                        <td>BUS091</td>
                        <td>BSS DIPLOMA IN CONSUMER BEHAVIOR</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>92</td>
                        <td>BUS092</td>
                        <td>BSS DIPLOMA IN GLOBAL BUSINESS ENVIRONMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>93</td>
                        <td>BUS093</td>
                        <td>BSS DIPLOMA IN MULTINATIONAL CORPORATIONS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>94</td>
                        <td>BUS094</td>
                        <td>BSS DIPLOMA IN INTERNATIONAL BUSINESS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>95</td>
                        <td>BUS095</td>
                        <td>BSS DIPLOMA IN ORGANIZATIONAL BEHAVIOR</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>96</td>
                        <td>BUS096</td>
                        <td>BSS DIPLOMA IN MANAGERIAL EFFECTIVENESS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>97</td>
                        <td>BUS097</td>
                        <td>BSS ADVANCED DIPLOMA IN NGO MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>98</td>
                        <td>BUS098</td>
                        <td>BSS DIPLOMA IN LIBRARY AND INFORMATION SCIENCES</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                     
                        <td>99</td>
                        <td>BUS099</td>
                        <td>BSS DIPLOMA IN EVENT MANAGEMENT TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>100</td>
                        <td>BUS100</td>
                        <td>BSS DIPLOMA IN INSURANCE AGENT</td>
                       <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                        
                        <td>101</td>
                        <td>BUS101</td>
                        <td>BSS POST DIPLOMA IN FOREIGN TRADE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>102</td>
                        <td>BUS102</td>
                        <td>BSS POST DIPLOMA IN INTERNATIONAL BUSINESS</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>103</td>
                        <td>BUS103</td>
                        <td>POST DIPLOMA IN MATERIAL MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>104</td>
                        <td>BUS104</td>
                        <td>POST DIPLOMA IN PERSONNEL MANAGEMENT & INDUSTRIAL RELATIONS</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>105</td>
                        <td>BUS105</td>
                        <td>POST DIPLOMA IN PRODUCTION & OPERATION MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>106</td>
                        <td>BUS106</td>
                        <td>POST DIPLOMA IN PRODUCTION MANAGEMENT CONTROL</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>107</td>
                        <td>BUS107</td>
                        <td>BSS POST DIPLOMA IN HUMAN RESOURCE MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>108</td>
                        <td>BUS108</td>
                        <td>BSS ADVANCED DIPLOMA IN COMMUNITY DEVELOPMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                     
                        <td>109</td>
                        <td>BUS109</td>
                        <td>BSS ADVANCED DIPLOMA IN WELFARE LAW</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>110</td>
                        <td>BUS110</td>
                        <td>BSS POST DIPLOMA IN MATERIAL MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>111</td>
                        <td>BUS111</td>
                        <td>BSS DIPLOMA IN LOGISTICS,SUPPLY CHAIN MANAGEMENT AND RETAIL MANAGEMNET</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>112</td>
                        <td>BUS112</td>
                        <td>BSS DIPLOMA IN SHIPPING MANAGEMENT AND EXPORT IMPORT (EXIM) MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>113</td>
                        <td>BUS113</td>
                        <td>BSS POST DIPLOMA IN LOGISTICS,SUPPLYCHAIN MANAGEMENT&RETAIL MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>114</td>
                        <td>BUS114</td>
                        <td>BSS POST DIPLOMA IN INTERNATIONAL SHIPPING MGMT & EXPORT,IMPORT MGMT</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>115</td>
                        <td>BUS115</td>
                        <td>POST DIPLOMA IN RETAIL MANAGEMENT</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>116</td>
                        <td>BUS116</td>
                        <td>BSS DIPLOMA IN FINANCIAL ACCOUNTING</td>
                          <td>THREE MONTHS</td>
                    </tr>                    
                </tbody>
            </table>
            <hr class="space m" />
              <h3 class=" text-center text-color">SPECIAL COURSES #4: FASHION COURSES</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                    <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>FS001</td>
                        <td>BSS DIPLOMA IN FASHION TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>FS002</td>
                        <td>BSS DIPLOMA IN COMPUTERISED FASHION DESIGNING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>FS003</td>
                        <td>CUSHION AND PILLOW MAKING</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>FS004</td>
                        <td>CHICKAN WORK</td>
                        <td>ONE MONTH</td>
                    </tr>
                      <tr>
                        
                        <td>5</td>
                        <td>FS005</td>
                        <td>APPLIQUE WORK</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>FS006</td>
                        <td>QUILT WORK</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                     
                        <td>7</td>
                        <td>FS007</td>
                        <td>MACHINE EMBROIDERY</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>FS008</td>
                        <td>HAND EMBROIDERY</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>9</td>
                        <td>FS009</td>
                        <td>WOOL KNITTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>FS010</td>
                        <td>LACE MAKING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                        
                        <td>11</td>
                        <td>FS011</td>
                        <td>DECORATIVE STITCHES</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>FS012</td>
                        <td>CROCHET WORK</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>FS013</td>
                        <td>BATIK PRINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>FS014</td>
                        <td>FABRIC PAINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>15</td>
                        <td>FS015</td>
                        <td>RUBBER PAINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>FS016</td>
                        <td>SAREE DESIGNING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>17</td>
                        <td>FS017</td>
                        <td>SMOKING WORKS</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>FS018</td>
                        <td>ELECTRIC EMBROIDERY MACHINE OPERATOR</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>19</td>
                        <td>FS019</td>
                        <td>TAILOR</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>FS020</td>
                        <td>COSTUME DESIGNING & DRESS MAKING SKILLS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                        
                        <td>21</td>
                        <td>FS021</td>
                        <td>BSS DIPLOMA IN DRESS MAKING TECHNIQUES</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>FS022</td>
                        <td>FASHION DESIGNING TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>FS023</td>
                        <td>HAIR DRESSING TECHNICIAN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>FS024</td>
                        <td>BSS DIPLOMA IN SHIRT & PANT MAKING TECHNIQUES</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>25</td>
                        <td>FS025</td>
                        <td>BSS DIPLOMA IN TAILORING TRAINER</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>FS026</td>
                        <td>BSS DIPLOMA IN GARMENT TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>27</td>
                        <td>FS027</td>
                        <td>BSS DIPLOMA IN COSTUME DESIGNING TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>FS028</td>
                        <td>FASHION DESIGNING ASSISTANT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>29</td>
                        <td>FS029</td>
                        <td>POST DIPLOMA IN COMPUTER AIDED FASHION DESIGNING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>FS030</td>
                        <td>BSS DIPLOMA IN COMPUTER AIDED PATTERN MAKING TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>31</td>
                        <td>FS031</td>
                        <td>BSS DIPLOMA IN HAIR DRESSING TECHNIQUES</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>FS032</td>
                        <td>BSS ADVANCED DIPLOMA IN FASHION & GARMENT TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>FS033</td>
                        <td>BSS ADVANCED DIPLOMA IN COMPUTER AIDED FASHION TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>FS034</td>
                        <td>ADVANCED DIPLOMA IN FASHION TECHNOLOGY & GARMENT TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>35</td>
                        <td>FS040</td>
                        <td>DIPLOMA IN FASHION TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>FS042</td>
                        <td>BSS DIPLOMA IN APPAREL MANUFACTURING&ADVANCE PATTERN MAKING TECHNOLOGY</td>
                          <td>THREE MONTHS</td>
                    </tr>
                </tbody>
            </table>
                        <hr class="space m" />
              <h3 class=" text-center text-color">SPECIAL COURSES #5: MEDIA COURSES</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                    <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>MSS001</td>
                        <td>BSS DIPLOMA IN DIGITAL PHOTOGRAPHY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>MSS002</td>
                        <td>BSS DIPLOMA IN VISUAL MEDIA</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>MSS003</td>
                        <td>DIGITAL VIDEOGRAPHY</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>MSS004</td>
                        <td>BSS DIPLOMA IN DIGITAL CINEMATOGRAPHY</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>5</td>
                        <td>MSS005</td>
                        <td>FILM EDITING – NON LINEAR EDITING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>MSS006</td>
                        <td>DARK ROOM TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>7</td>
                        <td>MSS007</td>
                        <td>BSS DIPLOMA IN SOUND ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>MSS008</td>
                        <td>BSS DIPLOMA IN DIRECTION</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                     
                        <td>9</td>
                        <td>MSS009</td>
                        <td>AUDIO – VISUAL EFFECTS TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>MSS010</td>
                        <td>BSS DIPLOMA IN CINEMA ACTING</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>11</td>
                        <td>MSS011</td>
                        <td>BSS DIPLOMA IN SCREEN PLAY WRITING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>MSS012</td>
                        <td>BSS DIPLOMA IN JOURNALISM & MASS COMMUNICATION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>MSS013</td>
                        <td>SOUND EDITING IN COMPUTER DUBBING-RECORDING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>MSS014</td>
                        <td>BSS DIPLOMA IN VISUAL COMMUNICATION</td>
                        <td>ONE YEARS</td>
                    </tr>
                      <tr>
                        
                        <td>15</td>
                        <td>MSS015</td>
                        <td>BSS ADVANCED DIPLOMA IN JOURNALISM AND MASS COMMUNICATION</td>
                        <td>ONE YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>MSS016</td>
                        <td>BSS ADVANCED DIPLOMA IN CINEMATOGRAPHY</td>
                        <td>ONE YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>17</td>
                        <td>MSS017</td>
                        <td>RADIO JOCKEY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>MSS018</td>
                        <td>DIPLOMA IN VISUAL COMMUNICATION</td>
                        <td>TWO YEARS</td>
                    </tr>
                     <tr>
                     
                        <td>19</td>
                        <td>MSS019</td>
                        <td>DIPLOMA IN DIRECTION & SCREEN PLAY WRITING</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>MSS020</td>
                        <td>DIPLOMA IN CINEMA ACTING</td>
                        <td>TWO YEARS</td>
                    </tr>
                     <tr>
                        
                        <td>21</td>
                        <td>MSS021</td>
                        <td>DIPLOMA IN DIGITAL CINEMATOGRAPHY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>MSS022</td>
                        <td>DIPLOMA IN EDITING,VFX & DIGITAL INTERMEDIATE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>MSS023</td>
                        <td>DIPLOMA IN ADVRTISING & MEDIA COMMUNICATION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>MSS024</td>
                        <td>DIPLOMA IN RADIO,VIDEO JOCKEY & NEWS READING</td>
                        <td>TWO YEARS</td>
                    </tr>
                      <tr>
                        
                        <td>25</td>
                        <td>MSS025</td>
                        <td>DIPLOMA IN DIGITAL CINEMA EDITING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>MSS026</td>
                        <td>DIPLOMA IN DIGITAL PHOTOGRAPHY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>27</td>
                        <td>MSS027</td>
                        <td>DIPLOMA IN EDITING, VFX</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>MSS028</td>
                        <td>DIPLOMA IN DIRECTION AND SCREENPLAY WRITING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>29</td>
                        <td>MSS029</td>
                        <td>BSS DIPLOMA IN RECORDING ARTS & MUSIC TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>MSS030</td>
                        <td>DIPLOMA IN ADVERITISING</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>31</td>
                        <td>MSS031</td>
                        <td>DIPLOMA IN CINEMATOGRAPHY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>MSS032</td>
                        <td>DIPLOMA IN VISUAL COMMUNICATION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>MSS033</td>
                        <td>BSS ADVANCED DIPLOMA IN DIRECTION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>MSS034</td>
                        <td>CERTIFICATE IN DIRECTION & SCRIPT WRITING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                </tbody>
            </table>
            <hr class="space m" />
              <h3 class=" text-center text-color">SPECIAL COURSES #6: SHIPPING COURSES</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                    <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>SS001</td>
                        <td>BSS DIPLOMA IN DIGITAL PHOTOGRAPHY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>SS002</td>
                        <td>BSS DIPLOMA IN INTERNATIONAL SHIPPING MANAGEMENT</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>SS003</td>
                        <td>BSS DIPLOMA IN CREW MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>SS004</td>
                        <td>BSS DIPLOMA IN SHIP SAFETY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>5</td>
                        <td>SS005</td>
                        <td>BSS DIPLOMA IN SHIP CATERING MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>SS006</td>
                        <td>BSS DIPLOMA IN PORT OPERATIONS & MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>7</td>
                        <td>SS007</td>
                        <td>BSS DIPLOMA IN CABIN ROOM TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>SS008</td>
                        <td>BSS DIPLOMA IN MARINE DIESEL ENGINEERING</td>
                        <td>TWO YEARs</td>
                    </tr>
                     <tr>
                     
                        <td>9</td>
                        <td>SS009</td>
                        <td>BSS DIPLOMA IN MARINE DIESEL ENGINE MECHANIC</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>SS011</td>
                        <td>CERTIFICATION ANSYS AQWA</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                        
                        <td>11</td>
                        <td>SS012</td>
                        <td>CERTIFICATION IN ANSYS FLUENT</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>SS013</td>
                        <td>CERTIFICATION IN CFD</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>SS014</td>
                        <td>CERTIFICATION IN COMPUTER APPLICATION IN DESIGN OF OFFSHORE STRUCTURE</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>SS015</td>
                        <td>CERTIFICATION IN COMPUTER APPLICATION IN HULL FORM DESIGN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                      <tr>
                        
                        <td>15</td>
                        <td>SS016</td>
                        <td>CERTIFICATION IN COMPUTER APPLICATION IN SEAKEEPING AND MANEUVERING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>SS017</td>
                        <td>CERTIFICATION IN COMPUTER APPLICATION IN STRUCTURAL ANALYSIS OF SHIPS</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>17</td>
                        <td>SS018</td>
                        <td>CERTIFICATION IN DESIGN OF SPECIAL PURPOSE VESSEL</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>SS019</td>
                        <td>CERTIFICATION IN ENGINEERING ECONOMICS IN SHIP DESIGN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                     
                        <td>19</td>
                        <td>SS020</td>
                        <td>CERTIFICATION IN MAXSURF, MOSES, SACS</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>SS021</td>
                        <td>CERTIFICATION IN SHIP DESIGN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                        
                        <td>21</td>
                        <td>SS022</td>
                        <td>CERTIFICATION IN SHIP RESISTANCE AND PROPULSION</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>SS023</td>
                        <td>CERTIFICATION IN SHIP STABILITY</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>SS025</td>
                        <td>DIPLOMA IN OIL RIG AND DRILLING PROCESS</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>SS026</td>
                        <td>DIPLOMA IN SHIP BUILDING AND OFFSHORE TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                      <tr>
                        
                        <td>25</td>
                        <td>SS027</td>
                        <td>DIPLOMA IN SHIP DRAUGHTSMAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>SS028</td>
                        <td>DIPLOMA IN SUBSEA & PIPELINE ENGINEERING</td>
                          <td>TWO YEARS</td>
                    </tr>
                </tbody>
            </table>
          </div>
    </div>
