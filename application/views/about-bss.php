<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About BSS | BSS</title>  
    <script src="<?php echo base_url();?>assets/HTWF/scripts/jquery.min.js"></script> 
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">  
    <link rel="icon" href="<?php echo base_url();?>assets/images/bss.png">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
 <script src="<?php echo base_url();?>assets/HTWF/scripts/jquery.min.js"></script>    
    <script src="<?php echo base_url();?>assets/HTWF/scripts/script.js"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/flexslider/flexslider.css">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/skin.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/HTWF/scripts/magnific-popup.css">
     <link rel="stylesheet" href="<?php echo base_url();?>assets/css/content-box.css">
</head>
<body>	
      
    <div id="about-pdf">
    <div class="section-empty">
        <div class="container content">
         <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
            <img src="<?php echo base_url();?>assets/images/aboutpdf/1.jpg">
         </div>
            </div>
          </div>
           <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/3.jpg">
         </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/5.jpg">
         </div>
            </div>
          </div> 
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/6.jpg">
         </div>
            </div>
          </div>           
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/7.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/8.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/9.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/10.jpg">
         </div>
            </div>
          </div> 
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/11.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/12.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/13.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/14.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/15.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/16.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/17.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/18.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/19.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/20.jpg">
         </div>
            </div>
          </div> 
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/21.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/22.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/23.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/24.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/25.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/26.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/27.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/28.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/29.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/30.jpg">
         </div>
            </div>
          </div> 
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/31.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/32.jpg">
         </div>
            </div>
          </div> 
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/33.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/34.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/35.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/36.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/37.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/38.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/39.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/40.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/41.jpg">
         </div>
            </div>
          </div> 
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/42.jpg">
         </div>
            </div>
          </div>  
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/43.jpg">
         </div>
            </div>
          </div>  
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/44.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/45.jpg">
         </div>
            </div>
          </div>  
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/46.jpg">
         </div>
            </div>
          </div>  
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/47.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/48.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/49.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/50.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/51.jpg">
         </div>
            </div>
          </div> 
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/52.jpg">
         </div>
            </div>
          </div>          
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/53.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/54.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/55.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/56.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/57.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/58.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/59.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/60.jpg">
         </div>
            </div>
          </div> 
        
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/61.jpg">
         </div>
            </div>
          </div> 
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/62.jpg">
         </div>
            </div>
          </div>          
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/63.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/64.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/65.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/66.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/67.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/68.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/69.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/70.jpg">
         </div>
            </div>
          </div> 
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/71.jpg">
         </div>
            </div>
          </div> 
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/72.jpg">
         </div>
            </div>
          </div>          
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/73.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/74.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/75.jpg">
         </div>
            </div>
          </div> 
         <div class="col-md-12">
            <div class="row">
              <div class="col-md-2"></div>
           <div class="col-md-8">
             <img src="<?php echo base_url();?>assets/images/aboutpdf/76.jpg">
         </div>
            </div>
          </div> 
    </div>
  </div>
</div>
</div>
  <i class="scroll-top scroll-top-mobile show fa fa-sort-asc"></i>
     
</body>
</html>
