<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home | BSS</title>    
  <script src="<?php echo base_url();?>assets/HTWF/scripts/jquery.min.js"></script> 
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">  
    <link rel="icon" href="<?php echo base_url();?>assets/images/bss.png">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
 <script src="<?php echo base_url();?>assets/HTWF/scripts/jquery.min.js"></script>    
    <script src="<?php echo base_url();?>assets/HTWF/scripts/script.js"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/flexslider/flexslider.css">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/skin.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/HTWF/scripts/magnific-popup.css">
     <link rel="stylesheet" href="<?php echo base_url();?>assets/css/content-box.css">
     
   
</head>
<body>	
 <header class="fixed-top scroll-change">
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
              <div class="navbar-mini scroll-hide">
                <div class="container">
                    <div class="nav navbar-nav navbar-left">
                        <span><i class="fa fa-phone"></i><a class="mobilesOnly" href="tel:+91 8547551095">+91 8547551095</a></span>
                        <hr />
                        <span><i class="fa fa-envelope"></i><a href="mailto:bssmalappuram@gmail.com">bssmalappuram@gmail.com</a></span>
                        </div>
                         <div class="nav navbar-nav navbar-right">
                         <div class="right-address">
                        <span >  <i class="fa fa-map-marker"></i>Pandikkad Road Manjeri</span>
                         </div>
                        <div class="right-skills">
                       <a href="my-skill-certificate"><span>MY SKILL CERTIFICATE </span></a>
                   </div>
                    </div>
                    
                </div>
            </div>
            <div class="navbar navbar-main">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="index">
                            <img class="logo-default" src="<?php echo base_url();?>assets/images/logo.png" alt="logo" />
                            <img class="logo-retina" src="<?php echo base_url();?>assets/images/logo.png" alt="logo" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                           
                                 <li class="dropdown <?php echo ($current == 'about-us' ? 'active' :'')?>">
                                <a href="about-us"role="button">About Us </a>
                                 </li>
                            <li class="dropdown <?php echo ($current == 'courses' ? 'active' :'')?>">
                                <a href="courses" role="button">Courses </a>
                               
                            </li>
                              <li class="dropdown <?php echo ($current == 'online-courses' ? 'active' :'')?>">
                                <a href="online-courses"role="button">Online Courses </a>
                                 </li>
                                  <li class="dropdown <?php echo ($current == 'become-a-bss-centre' ? 'active' :'')?>">
                                <a href="become-a-bss-centre"role="button">Become a BSS Centre </a>
                                 </li>
                                  <li class="dropdown <?php echo ($current == 'become-an-associate' ? 'active' :'')?>">
                                <a href="become-an-associate"role="button">Become an Associate </a>
                                 </li>
                                  <li class="dropdown <?php echo ($current == 'approved-centers' ? 'active' :'')?>">
                                <a href="approved-centers"role="button">Approved Centers </a>
                                 </li> 
                                  <li class="dropdown <?php echo ($current == 'gallery' ? 'active' :'')?>">
                               <a href="gallery"role="button">Gallery </a>
                                 </li>
                                  <li class="dropdown <?php echo ($current == 'contact-us' ? 'active' :'')?>">
                                <a href="contact-us"role="button">Contact Us </a>
                                 </li> 

                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </header>