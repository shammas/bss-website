 <i class="scroll-top scroll-top-mobile show fa fa-sort-asc"></i>
     <footer class="footer-base">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 footer-center text-left">
                     <h3 class="text-color"> BHARAT SEVAK SAMAJ</h3>
                        
                        <p class="text-s">Bharat Sevak Samaj is National Development Agency,Established in 1952 by Planning Commission,Government of India to ensure public co-operation for implementing Government plans.</p>                    
                    </div>
                <div class="col-md-4 footer-left text-left">
                        <div class="row">
                            <div class="col-md-6 text-s">
                                <h3>Menu</h3>
                                <a href="index">Home</a><br />
                                <a href="about-us">About Us</a><br />
                                <a href="online-courses">Online Courses</a><br />
                                <a href="become-a-bss-centre">Become a BSS Centre</a><br />
                                <a href="approved-centers">Approved Centers </a><br />
                               <a href="gallery">Gallery </a><br />
                                <a href="contact-us">Contact Us</a><br />
                            </div>
                            <div class="col-md-6 text-s">
                                <h3>Courses</h3>
                                <a href="medical-technology-courses">Medical Technology Courses</a><br />
                                <a href="category-courses">Category Courses</a><br />
                                <a href="school-courses">School Courses</a><br />
                                <a href="special-courses">Special Courses</a><br />
                               
                            </div>
                        </div>
                    </div>
                   <div class="col-md-4 footer-left text-left">
                        <h3>Contact Us</h3>
                        <p class="text-s"> Malappuram District HO 3rd Floor, Korambayil Arcade</p>
                       <p class="text-s">Pandikkad Road Manjeri – 676 121</p>

                        <div class="tag-row text-s">
                            <span><a href="mailto:bssmalappuram@gmail.com">bssmalappuram@gmail.com</a><br></span>

                            <span> <a class="mobilesOnly" href="tel:+91 8547551095">+91 8547551095</a><br></span>
                            <span><a class="mobilesOnly" href="tel:0483 2762595">0483 2762595</a><br></span>

                       </div>
                    </div>
                </div>
            </div>
           <div class="row copy-row ">
                <div class="col-md-6 text-left">
                  Copyright © <script>document.write(new Date().getFullYear())</script> <a href="index">BSS</a>. All Rights Reserved. 
                </div>
                <div class="col-md-6 text-right">
                   Designed and Maintained by  <a href="http://cloudbery.com/">  <img src="<?php echo base_url();?>assets/images/Cloudbery.png" alt="" /></a>
                </div>
            </div>
        </div>
        <script async src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>        
        <script src="<?php echo base_url();?>assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="<?php echo base_url();?>assets/HTWF/scripts/jquery.twbsPagination.min.js"></script>
        <script src="<?php echo base_url();?>assets/HTWF/scripts/jquery.magnific-popup.min.js"></script>
    </footer>
</body>
</html>

