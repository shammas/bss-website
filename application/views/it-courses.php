
     <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>IT Courses</h1>
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
        <div class="container content">          
            <hr class="space s" />
            <h3 class=" text-center text-color">BSS DIPLOMA COURSES</h3>
            <hr class="space m" />
             <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>POST GRADUATE DIPLOMA IN COMPUTER APPLICATIONS (PGDCA)</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>DIPLOMA IN COMPUTER TEACHERS TRAINING COURSE (DCTTC)</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>                       
                        <td>ADVANCED DIPLOMA IN LAPTOP TECHNOLOGY & CHIP-LEVEL SERVICING (ADLTCS)</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>DIPLOMA IN LAPTOP CHIP-LEVEL SERVICING (DLCS)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>DIPLOMA IN 3D ANIMATION </td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>DIPLOMA IN OFFICE AUTOMATION (DOA)</td>
                        <td>FOUR MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>DIPLOMA IN COMPUTER HARDWARE AND NETWORKING (DCHN)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>DIPLOMA IN COMPUTERISED FINANCIAL MANAGEMENT (DCFM)</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>DIPLOMA IN COMPUTERISED SECRETARIAL PRACTICE  (DCSP)</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>DIPLOMA IN COMPUTER HARDWARE MAINTENANCE AND NETWORK ENGINEERING (DCHMN)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>DIPLOMA IN MULTIMEDIA</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>DIPLOMA IN GRAPHICS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>DIPLOMA IN COMPUTER HARDWARE AND NETWORK ENGINEERING (DCNE)</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>DIPLOMA IN SOFTWARE ENGINEERING (DSE)</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>MULTIMEDIA ANIMATION COMPUTER TEACHER EDUCATION (MACTE)</td>
                        <td>TEN MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>DIPLOMA IN COMPUTER TEACHER EDUCATION (DCTE)</td>
                        <td>TEN MONTHS</td>
                    </tr>
                </tbody>
            </table>
            <hr class="space s" />
            <h3 class=" text-center text-color">BSS DIPLOMA COURSES</h3>
            <hr class="space m" />
             <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>CERTIFICATE IN INFORMATION TECHNOLOGY (CIT)</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>CERTIFICATE IN ELECTRONIC OFFICE (CEO)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>                       
                        <td>CERTIFICATE IN COMPUTER HARDWARE MAINTENANCE </td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>CERTIFICATE IN DATA ENTRY AND CONSOLE OPERATION</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>CERTIFICATE IN WEB DESIGNING (CWD)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>CERTIFICATE IN DESKTOP PUBLISHING (CDTP)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>CERTIFICATE IN MOBILE PHONE TECHNOLOGY</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>2D ANIMATION</td>
                          <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>PREPARATORY COURSE IN ELECTRONIC OFFICE</td>
                          <td>FOUR MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>PREPARATORY COURSE IN DTP</td>
                        <td>FOUR MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>CERTIFICATE IN MS-OFFICE</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>CERTIFICATE IN VISUAL BASIC</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>CERTIFICATE IN C++ PROGRAMMING</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>CERTIFICATE IN OPEN OFFICE</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>CERTIFICATE IN EXCEL</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>CERTIFICATE IN WINDOWS/LINUX</td>
                        <td>ONE MONTH</td>                  
                
                    <tr>
                        
                        <td>17</td>
                        <td>CERTIFICATE IN ADOBE PHOTOSHOP</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>CERTIFICATE IN ORACLE</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>                       
                        <td>CERTIFICATE IN VISUAL C++ </td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>CERTIFICATE IN C PROGRAMMING</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>CERTIFICATE IN INTERNET APPLICATION</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>CERTIFICATE IN JAVA PROGRAMMING</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>23</td>
                        <td>CERTIFICATE IN COMPUTERIZED ACCOUNTING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>CERTIFICATE IN ADOBE PAGEMAKER</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                     
                        <td>25</td>
                        <td>CERTIFICATE IN COREL DRAW</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>CERTIFICATE IN ASP.NET</td>
                        <td>FOUR MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>27</td>
                        <td>CERTIFICATE IN PHP</td>
                          <td>FOUR MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>CERTIFICATE IN C#.NET</td>
                        <td>FOUR MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>CERTIFICATE IN VB.NET</td>
                        <td>FOUR MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>CERTIFICATE IN AUTO CAD</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>31</td>
                        <td>CERTIFICATE IN WORD PROCESSING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>CERTIFICATE IN GRAPHIC DESIGN</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>33</td>
                        <td>2D AND 3D ANIMATION</td>
                        <td>FOUR MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>CERTIFICATE IN AUDIO AND VIDEO EDITING</td>
                        <td>FOUR MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>CCA</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>36</td>
                        <td>CFA</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>37</td>
                        <td>CCFA</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <hr class="space s" />
            <h3 class=" text-center text-color">CCTV INSTALLATION & MAINTENANCE</h3>
            <hr class="space m" />
             <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>CCTV INSTALLATION & MAINTENANCE </td>
                          <td></td>
                    </tr>
                </tbody>
            </table>
            <hr class="space s" />
            <h3 class=" text-center text-color">PSC APPROVED COURSES</h3>
            <hr class="space m" />
             <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>POST GRADUATE DIPLOMA IN COMPUTER APPLICATION(FAST TRACK) (PGDCA)</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>DIPLOMA IN COMPUTER APPLICATION (DCA)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>                       
                        <td>COMPUTER TEACHER TRAINING PROGRAM (CTTP)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>DATA ENTRY OPERATOR & OFFICE AUTOMATION (DEOA)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>DIPLOMA IN FINANCIAL & FOREIGN ACCOUNTING (DFFA)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>MS OFFICE</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>OFFICE AUTOMATION</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>TALLY</td>
                        <td>THREE MONTHS</td>
                    </tr>
                </tbody>
            </table>
          </div>
    </div>

 