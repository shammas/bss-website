
    <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1 >Approved Centers</h1>                
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
        <div class="container content container-xs">
           <hr class="space s" />
             <h2 class=" aligncenter text-color">LIST OF AUTHORISED TRAINING CENTERS & INSTITUTIONAL MEMBERS</h2>
             <hr class="space s" />
            <table class="table">
                <thead>
                    <tr>
                       <th>SNO</th>
                       <th>AUTHORISED TRAINING CENTERS & INSTITUTIONAL MEMBERS</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($centers) and $centers) {
                        foreach ($centers as $key=>$center) {
                        ?>
                        <tr>
                            <td><?php echo $key+1;?></td>
                            <td><?php echo $center->name;?></td>
                        </tr>
                        <?php 
                        }
                    }
                    ?>    
                </tbody>
            </table>
            
          </div>
    </div>
  