
    <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>Contact Us</h1>
            </div>
        </div>
    </div> 
    <div class="section-empty section-item contact-sec">
        <div class="container content">
            <div class="grid-list gallery list-sm-6">
                <div class="grid-box row">
                    <div class="grid-item col-md-3 col-sm-6">
                        <div class="advs-box advs-box-top-icon boxed-inverse no-icon">
                            <h4 class="text-color">The Planning Council Secretary</h4>

                            <p>
                                MR. NASIRUDDEN ALUNGAL
                                 Bharat Sevak Samaj
                                   3rd Floor, Korambayil Arcade
                                   Pandikkad Road, Manjeri – 676 121
                                   Kerala, India
                            </p>
                           
                        </div>
                    </div>
                    <div class="grid-item col-md-3 col-sm-6">
                        <div class="advs-box advs-box-top-icon boxed-inverse no-icon">
                            <h4 class="text-color">HON. Chairman</h4>
                           
                            <p>
                                 DR. B.S. BALACHANDRAN,
                                    Central Bharat Sevak Samaj                                  
                                    New Delhi, India
                            </p>
                            
                        </div>
                    </div>
                    <div class="grid-item col-md-3 col-sm-6">
                        <div class="advs-box advs-box-top-icon boxed-inverse no-icon">
                            <h4 class="text-color">Central Office</h4>
                           
                            <p>
                                Bharat Sevak Samaj
                                    BSS Bhavan, BB-11                                  
                                    Greater Kailash - 3                                   
                                    New Delhi - 110 048, India
                            </p>
                          
                        </div>
                    </div>
                    <div class="grid-item col-md-3 col-sm-6">
                        <div class="advs-box advs-box-top-icon boxed-inverse no-icon">
                            <h4 class="text-color">Central Programme Office</h4>
                            <p>
                                Bharat Sevak Samaj
                                     Satbhavana Bhavan, Brahmins Colony                                 
                                    Kowdiar P.O, Trivandrum - 695003                                   
                                    Kerala, India.
                            </p>
                            
                        </div>
                    </div> 
                </div>                
            </div>
            <hr class="space m" />
             <div class="row">
        <div class="col-md-6">
                    <h2 class="text-color">Send a message</h2>
                     <hr class="space s" />
                      <p>We reply within 48 hour.</p>
                    <hr class="space s" />                    
                    <!-- <form action="#" > -->
                        <?php echo form_open_multipart('contact_us_mail', ['id' => 'form','name' => 'contactForm']);
                                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Name</p>
                                <input id="name" name="name" placeholder="" type="text" class="form-control form-value" required>
                            </div>
                            <div class="col-md-6">
                                <p>Email</p>
                                <input id="email" name="email" placeholder="" type="email" class="form-control form-value" required>
                            </div>
                        </div>
                        <hr class="space s" />
                             <div class="row">
                            <div class="col-md-6">
                               <p>Mobile No</p>
                                <input id="mobileno" name="mobileno" placeholder="" type="tel" class="form-control form-value" required>  
                            </div>
                            <div class="col-md-6">
                               <p>WhatsApp No</p>
                                <input id="whatsappno" name="whatsappno" placeholder="" type="tel" class="form-control form-value" required>  
                            </div>
                          </div>
                        <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                                 <hr class="space s" />
                                  <p>Subject</p>
                                  <input id="subject" name="subject" class="form-control form-value" required></input>
                                   <hr class="space s" />
                                <p>Message</p>
                               
                                <textarea id="message" name="message" class="form-control form-value" required></textarea>
                                <hr class="space s" />
                                <button class="anima-button btn-border btn-sm btn" type="submit"><i class="fa fa-mail-reply-all"></i>Send message</button>
                            </div>
                        </div>
                       
                    </form>
                </div>

        <div class="col-md-6">
              <hr class="space visible-xs" /> 
                    <h2 class="text-color">How to reach us</h2>                    
                    <hr class="space s" />
                    <p>You can reach by follow the directions</p>
                    <hr class="space s" />
                                        
                                                     
                              <p><a class="mobilesOnly" href="tel:+91 8547551095">+91 8547551095</a></p>
                               <p><a class="mobilesOnly" href="tel:0483 2762595">0483 2762595</a></p> 
                                <p></p><a href="mailto:bssmalappuram@gmail.com">bssmalappuram@gmail.com</a></div>
                               
                           
                    </div>
        </div>
    </div>
        </div>
       
</div>
