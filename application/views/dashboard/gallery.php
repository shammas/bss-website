<div class="wrapper wrapper-content animated fadeInRight"  >
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : !files, 'col-lg-9' : files}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add gallery</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" ng-submit="addGallery()" >
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Name</label>
                            <div class="col-lg-10"  ng-class="{'has-error' : validationError.name}">
                                <input type="text" placeholder="Your Name here" class="form-control" ng-model="newgallery.name">
                            </div>
                        </div>
                        
                        <div class="form-group" ng-class="{'has-error' : validationError.file}">
                            <label for="" class="control-label col-lg-2">Photo</label>
                            <div class="col-md-10">
                                <button ngf-select="uploadFiles($files, $invalidFiles)"
                                        accept="image/*"
                                        ngf-max-height="5000"
                                        ngf-max-size="5MB"
                                        ngf-multiple="true" type="button"
                                        class="upload-drop-zone btn-default"
                                        ngf-drop="uploadFiles($files)"
                                        ngf-drag-over-class="'drop'" ngf-multiple="false"
                                        ngf-pattern="'image/*'"
                                        ng-class="{'upload-drop-zone-error' : validationError.file}">
                                    <div class="dz-default dz-message">
                                        <span><strong>Drop files here or click to upload. </strong></span>
                                    </div>
                                </button>
                                <div class="help-block" ng-show="validationError.file">Please select image!</div>
                                <span class="alert alert-danger" ng-show="fileValidation.status == true">{{fileValidation.msg}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit"  ng-bind="(curgallery == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>
                    <div class="lightBoxGallery" ng-show="curgallery">
                        <a class="example-image-link" href="{{curgallery.url + curgallery.file_name}}" data-lightbox="item-list" data-title="">
                            <img src="{{curgallery.url + curgallery.file_name}}" width="100px">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3" ng-show="files">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload status</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <h5>{{files.name}}</h5>
                        <div class="lightBoxGallery">
                            <a class="example-image-link" href="{{files.$ngfBlobUrl}}" data-lightbox="example-1" data-title="">
                                <img ngf-src="files.$ngfBlobUrl" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                            </a>
                        </div>
                        <div class="progress">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar"
                                 class="progress-bar progress-bar-success"
                                 style="width:{{files.progress}}%" ng-show="uploadstatus != 1">
                                <span>{{files.progress}}% Complete</span>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" ng-repeat="f in errFiles">{{files.name}} {{files.$error}} {{files.$errorParam}}.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All galleries</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="">
                        <button type="button" class="btn btn-primary" ng-click="newGallery()">
                            Add a new gallery
                        </button>
                    </div>
                    <div class="table-responsive">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="dataTables_length" id="DataTables_Table_0_length">
                                <label>
                                    <select  aria-controls="DataTables_Table_0" class="form-control input-sm" ng-model="numPerPage"
                                             ng-options="num for num in paginations">{{num}}
                                    </select>
                                    entries
                                </label>
                            </div>
                            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                <label>Search:<input class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0" type="search" ng-model="search"></label>
                            </div>
                            <table class="table table-striped table-bordered table-hover dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
                                <thead>
                                <tr role="row">
                                    <th>Sl No</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="gradeA odd" role="row" dir-paginate="gallery in galleries | filter:search | limitTo:pageSize | itemsPerPage:numPerPage" current-page="currentPage">

                                    <td>{{$index+1}}</td>
                                    <td>{{gallery.name}}</td>
                                    <td class="center">
                                        <a class="example-image-link" href="{{gallery.url + gallery.file_name}}" data-lightbox="example-1" data-title="">
                                            <img src="{{gallery.url + gallery.file_name}}" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                                        </a>
                                    </td>
                                    <td class="center">
                                        <div  class="btn-group btn-group-xs" role="group">
                                            <button type="button" class="btn btn-info" ng-click="editGallery(gallery)">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button  type="button" class="btn btn-danger" ng-click="deleteGallery(gallery)">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-4">

                                <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                    Showing {{currentPage}} to {{(numPerPage < galleries.length  ? currentPage*numPerPage :galleries.length)}} of {{galleries.length}} entries
                                </div>
                                <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                    Showing {{(currentPage-1)*numPerPage+1}} to {{(currentPage*numPerPage)}} of {{galleries.length}} entries
                                </div>
                            </div>



                            <div class="col-md-8 pull-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true">
                                    </dir-pagination-controls>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
