
    <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>Online Courses </h1>
            </div>
        </div>
    </div>
            <div class="container content container-xs">
            <div class="row">
                <div class="col-md-12">          
                   <hr class="space s" />
                       <h2 class="aligncenter text-color">Application for Online Courses</h2>
                    <p class="aligncenter">
                       Online Courses are available for Medical Technology Courses, Category Courses, School Courses, Special Courses, and IT Courses. For details and admission fill-up and submit the following form.
                    </p>
                    <hr class="space s" />
                    <!-- <form action="#" > -->
                      <?php echo form_open_multipart('online_courses_application', ['id' => 'form','name' => 'contactForm']);?>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Full Name</p>
                                <input id="name" name="name" placeholder="" type="text" class="form-control form-value" required>
                            </div>
                          </div>
                          <hr class="space s" />
                             <div class="row">
                            <div class="col-md-4">
                               <p>Age</p>
                                <input id="age" name="age" placeholder="" type="number" class="form-control form-value" required>  
                            </div>
                             <div class="col-md-4">
                              <p>Date of birth</p>
                              <input id="dob" name="dob" placeholder="" type="date" class="form-control form-value" required>

                        </div>
                        <div class="col-md-4">
                              <p>Sex</p>
                              <input id="sex" name="sex" placeholder="" type="text" class="form-control form-value" required>
                        </div>
                      </div>
                      <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>Place</p>
                                <input id="place" name="place" placeholder="" type="text" class="form-control form-value" required>                               
                            </div>
                        </div>
                         <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>   Educational Qualification</p>
                                <input id="education" name="education" placeholder="" type="text" class="form-control form-value" required>                               
                            </div>
                        </div>
                     <hr class="space s" />
                             <div class="row">
                            <div class="col-md-6">
                               <p>Mobile No</p>
                                <input id="mobileno" name="mobileno" placeholder="" type="tel" class="form-control form-value" required>  
                            </div>
                            <div class="col-md-6">
                               <p>WhatsApp No</p>
                                <input id="whatsappno" name="whatsappno" placeholder="" type="tel" class="form-control form-value" required>  
                            </div>
                          </div>
                       <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>Email</p>
                                <input id="email" name="email" placeholder="" type="email" class="form-control form-value" required>
                               
                            </div>
                        </div>
                        <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p> Course Name </p>
                                 <input id="course" name="course" placeholder="" type="text" class="form-control form-value" required> 
                               
                            </div>
                        </div>
                        <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                                <p>Message</p>
                                <textarea id="message" name="message" class="form-control form-value" required></textarea>
                                <hr class="space s" />
                                <button class="anima-button btn-border btn-sm btn" type="submit"><i class="fa fa-mail-reply-all"></i>Submit Application</button>
                            </div>
                        </div>
                       
                    </form>
                </div>
               
            </div>
        </div>
    </div>
