
     <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>School Courses</h1>
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
       <div class="container content">
            <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #2: ACUPUNCTURE SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>ACS001</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & AYURVEDA</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>ACS002</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & NATUROPATHY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>ACS003</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & HOMEOPATHY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>ACS004</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & SIDDHA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>ACS005</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>ACS006</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>ACS007</td>
                        <td>BSS DIPLOMA IN PHYSIOTHERAPY AND ACUPRESSURE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>ACS008</td>
                        <td>CERTIFICATE IN ACUPUNCTURE FIRST-AID</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>ACS009</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & TRADITIONAL PLUSE DIAGNOSING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>ACS010</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & PSYCHOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>ACS011</td>
                        <td>CERTIFICATE IN SUJOK ACUPUNCTURE</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>ACS012</td>
                        <td>CERTIFICATE IN AROMATHERAPY AND COSMETIC ACUPUNCTURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>ACS013</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE THERAPY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>ACS014</td>
                        <td>CERTIFICATE IN MAGNETO THERAPY AND ACUPRESSURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>ACS015</td>
                        <td>CERTIFICATE IN REFLEXOLOGY AND ACUPRESSURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>ACS016</td>
                        <td>CERTIFICATE IN SHIATSZU MASSAGE AND AROMATHERAPY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>17</td>
                        <td>ACS017</td>
                        <td>CERTIFICATE IN IRIDOLOGY AND ACUPRESSURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>ACS018</td>
                        <td>CERTIFICATE IN FENGSHUI AND ACUPUNCTURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>ACS019</td>
                        <td>CERTIFICATE IN SCALP ACUPUNCTURE</td>
                       <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>ACS020</td>
                        <td>CERTIFICATE IN STRESS MANAGEMENT BY ACUPUNCTURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                       <td>21</td>
                        <td>ACS021</td>
                        <td>CERTIFICATE IN DE-ADDICTION BY ACUPUNCTURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>ACS022</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & VARMA</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>ACS024</td>
                        <td>CERTIFICATE IN HOLOGRAPHY AND ACUPUNCTURE</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>ACS025</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & SIDDHA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>ACS026</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPUNCTURE HEALING TECHNIQUES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>ACS027</td>
                        <td>BSS DIPLOMA IN VARMANIAM MASSAGE THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>27</td>
                        <td>ACS028</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPUNCTURE AND VARMA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>ACS029</td>
                        <td>BSS DIPLOMA IN INTEGRATIVE ALTERNATIVE THERAPIES</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>ACS030</td>
                        <td>BSS ADVANCED CERTIFICATION IN ACUPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>ACS031</td>
                        <td>BSS DIPLOMA IN TRADITIONAL ACUPUNCTURE AND SUJOK ACUPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>31</td>
                        <td>ACS032</td>
                        <td>BASIC CERTIFICATION IN ACUPUNCTURE EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>ACS033</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE & YOGA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>ACS034</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPUNCTURE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>ACS035</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPUNCTURE THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>ACS036</td>
                        <td>BSS DIPLOMA IN ACUPUNCTURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>ACS037</td>
                        <td>ADVANCED CERTIFICATION IN CLASSICAL ACUPUNCTURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>37</td>
                        <td>ACS038</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>ACS039</td>
                        <td>CERTIFICATION IN ACUPUNCTURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>39</td>
                        <td>ACS040</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPRESSURE</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>ACS041</td>
                        <td>BSS DIPLOMA IN HOMEOPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>41</td>
                        <td>ACS042</td>
                        <td>CERTIFICATE IN ACU YOGA THERAPY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>ACS043</td>
                        <td>BSS DIPLOMA IN GUASA THERAPY & ART OF FOOT NEUROLOGICAL THERAPY</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>ACS044</td>
                        <td>CERTIFICATE IN ACUPUNCTURE MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>ACS045</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>45</td>
                        <td>ACS046</td>
                        <td>BSS DIPLOMA IN GUASA THERAPY & ART OF FOOT NEUROLOGICAL THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>ACS047</td>
                        <td>BSS DIPLOMA IN REFLEXOLOGY AND ACU TOUCH</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>47</td>
                        <td>ACS048</td>
                        <td>DIPLOMA IN ACUPUNCTURE & PHYSIOTHERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>ACS051</td>
                        <td>BSS ADVANCED DIPLOMA IN ACCUPUNCTURE & ACUPRESSURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>49</td>
                        <td>ACS052</td>
                        <td>BSS ADVANCED DIPLOMA IN ACUPUNCTURE AND NATUROPATHY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>50</td>
                        <td>ACS053</td>
                        <td>BSS DIPLOMA IN TIBB ACUPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>                    
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #3: AYURVEDA SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>AYS001</td>
                        <td>BSS DIPLOMA IN PHYSIOTHERAPY & AYURVEDA</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>AYS002</td>
                        <td>BSS DIPLOMA IN AYURVEDA SCIENCE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>AYS003</td>
                        <td>BSS DIPLOMA IN AYURVEDA NURSING & PHARMACY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>AYS004</td>
                        <td>BSS DIPLOMA IN PANCHAKARMA & MASSAGE THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>AYS005</td>
                        <td>BSS DIPLOMA IN AYURVEDA NURSING</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>AYS006</td>
                        <td>BSS DIPLOMA IN AYURVEDIC BEAUTY CARE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>AYS007</td>
                        <td>CERTIFICATE IN DIABETIC CARE & DIET BY AYURVEDA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>AYS008</td>
                        <td>BSS DIPLOMA IN AYURVEDA MASSAGE & PANCHAKARMA THERAPY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>AYS009</td>
                        <td>BSS DIPLOMA IN AYURVEDA, PANCHAKARMA , NURSING & YOGA</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>AYS010</td>
                        <td>CERTIFICATE IN ARTHRITIC CARE & DIET BY AYURVEDA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>AYS011</td>
                        <td>CERTIFICATE IN OBESITY CARE BY AYURVEDA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>AYS012</td>
                        <td>CERTIFICATE IN SPA MASSAGE BY AYURVEDA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>AYS013</td>
                        <td>CERTIFICATE IN AYURVEDIC BEAUTY CARE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>AYS015</td>
                        <td>CERTIFICATE IN AYURVEDA MASSAGE & TREATMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>AYS016</td>
                        <td>CERTIFICATE IN AYURVEDA NURSING & PANCHAKARMA THERAPY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>AYS017</td>
                        <td>CERTIFICATE IN AYURVEDA NURSING & PHARMACY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>17</td>
                        <td>AYS018</td>
                        <td>CERTIFICATE IN AYURVEDA & PANCHAKARMA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>AYS019</td>
                        <td>BSS DIPLOMA IN AYURVEDA, PANCHAKARMA & NURSING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>AYS020</td>
                        <td>BSS DIPLOMA IN PANCHAKARMA</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>AYS021</td>
                        <td>BSS DIPLOMA IN KALARIUZHICHIL</td>
                        <td>ONE YEAR</td>
                    </tr>
                       <td>21</td>
                        <td>AYS022</td>
                        <td>BSS DIPLOMA IN MASSEUR & PANCHAKARMA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>AYS023</td>
                        <td>BSS DIPLOMA IN AYURVEDA PHARMACY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>AYS024</td>
                        <td>BSS DIPLOMA IN MASSEURA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>AYS025</td>
                        <td>CERTIFICATE IN AYURVEDA NURSING ASSISTANT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>AYS026</td>
                        <td>CERTIFICATE IN AYURVEDHA PHARMACY ASSISTANT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>AYS027</td>
                        <td>BSS DIPLOMA IN VARMA & MASSAGE SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>27</td>
                        <td>AYS028</td>
                        <td>BSS DIPLOMA IN AYURVEDA SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>AYS030</td>
                        <td>BSS DIPLOMA IN AYURVEDA & PANCHAKARMA</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>AYS031</td>
                        <td>BSS DIPLOMA IN AYURVEDA MASSAGE & TREATMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>AYS032</td>
                        <td>BSS DIPLOMA IN AYURVEDA SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>31</td>
                        <td>AYS034</td>
                        <td>BSS ADVANCED DIPLOMA IN WELLNESS THERAPY & MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>AYS035</td>
                        <td>BSS ADVANCED DIPLOMA IN PANCHAKARMA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>AYS037</td>
                        <td>BSS DIPLOMA IN AYURVEDIC GINECOLOGY & OBSTETRICS</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>AYS039</td>
                        <td>BSS DIPLOMA IN AYURVEDIC COSMETOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>AYS042</td>
                        <td>BSS DIPLOMA IN AYURVEDIC CHILD HEALTH</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>AYS043</td>
                        <td>BSS DIPLOMA IN AYURVEDIC CLINICAL PATHOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>37</td>
                        <td>AYS044</td>
                        <td>BSS DIPLOMA IN AYURVEDIC BEAUTY THERAPIES & AYURVEDA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>AYS045</td>
                        <td>BSS ADVANCED DIPLOMA IN AYURVEDA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>39</td>
                        <td>AYS046</td>
                        <td>BSS DIPLOMA IN AYURVEDA, PANCHAKARMA & NURSING</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>AYS047</td>
                        <td>BSS DIPLOMA IN BEAUTY AND SPA THERAPIES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>41</td>
                        <td>AYS048</td>
                        <td>ADVANCED DIPLOMA IN AYURVEDIC HERBAL PREPARATIONS</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>AYS049</td>
                        <td>BSS DIPLOMA IN HOSLISTIC SPA THERAPIES</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>AYS050</td>
                        <td>BSS DIPLOMA IN INTEGRATED SPA THERAPIES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>AYS051</td>
                        <td>BSS DIPLOMA IN SPA MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>45</td>
                        <td>AYS055</td>
                        <td>BSS DIPLOMA IN AYURVEDA KSHARSUTRA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>AYS060</td>
                        <td>BSS DIPLOMA IN HOLISTIC THERAPIES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>47</td>
                        <td>AYS063</td>
                        <td>CERTIFICATE IN AYURVEDIC FOOD SCIENCE AND COOKING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>AYS066</td>
                        <td>BSS DIPLOMA IN AYURVEDIC GENERAL PRACTICES</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>49</td>
                        <td>AYS067</td>
                        <td>BSS DIPLOMA IN AYURVEDIC LIFESTYLE COACH</td>
                          <td>SIX MONTHS</td>
                    </tr>                    
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #4: BEAUTY SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>BS001</td>
                        <td>BSS DIPLOMA IN COSMETOLOGY AND BEAUTY PARLOUR MANAGEMENT</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>BS002</td>
                        <td>BSS DIPLOMA IN BEAUTY TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>BS003</td>
                        <td>NATURE CURE BEAUTY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>BS004</td>
                        <td>SELF GROOMING & BRIDAL MAKE UP</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>BS005</td>
                        <td>HENNA MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>BS006</td>
                        <td>MEHANDI APPLICATION</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>BS007</td>
                        <td>BOUQUET MAKING & FLOWER DECORATION</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>BS008</td>
                        <td>COSMETOLOGY & BEAUTY PARLOUR MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>BS009</td>
                        <td>ADVANCE DIPLOMA IN SPA & BEAUTY</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>BS010</td>
                        <td>BSS DIPLOMA IN NAIL TECHNOLOGY</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>BS011</td>
                        <td>ADVANCED DIPLOMA IN HAIR STYLING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>BS012</td>
                        <td>BSS DIPLOMA IN HAIR DRESSING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>BS013</td>
                        <td>ADVANCED DIPLOMA IN ANTI AGING FACIAL AESTHETICS AND COSMETOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>BS014</td>
                        <td>BSS DIPLOMA IN COSMETOLOGY AND AESTHETICS</td>
                        <td>ONE YEAR</td>
                    </tr>
                    </tbody>
                  </table>
                  <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #6: FIRE & SAFETY SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>FSS001</td>
                        <td>BSS DIPLOMA IN FIRE & SAFETY METHODS</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>FSS002</td>
                        <td>BSS DIPLOMA IN FOOD SAFETY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>FSS003</td>
                        <td>BSS DIPLOMA IN INDUSTRIAL SAFETY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>FSS004</td>
                        <td>FIRE TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>FSS005</td>
                        <td>BSS DIPLOMA IN ELECTRICAL SAFETY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>FSS006</td>
                        <td>BSS DIPLOMA IN OFF SHORE SAFETY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>FSS007</td>
                        <td>BSS DIPLOMA IN FIRE FIGHTING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>FSS00TWO YEARS8</td>
                        <td>BSS DIPLOMA IN CONSTRUCTION SAFETY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>FSS009</td>
                        <td>BSS DIPLOMA IN ENVIRONMENTAL SAFETY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>FSS011</td>
                        <td>CERTIFICATE IN FIRE & SAFETY ENGINEERING TECHNIQUES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>FSS012</td>
                        <td>POST DIPLOMA FIRE & SAFETY ENGINEERING TECHNIQUES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>FSS013</td>
                        <td>BSS DIPLOMA IN FIRE ENGINEERING & SAFETY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>FSS014</td>
                        <td>ADVANCED DIPLOMA IN OCCUPATIONAL SAFETY,HEALTH & ENVIRONMENTAL MGMT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>FSS015</td>
                        <td>BSS POST DIPLOMA IN ENVIRONMENT SAFETY ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>FSS016</td>
                        <td>BSS DIPLOMA IN INDUSTRIAL ENVIRONMENTAL SAFETY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>FSS017</td>
                        <td>BSS DIPLOMA IN HEALTH , ENVIRONMENT & SAFETY ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>17</td>
                        <td>FSS018</td>
                        <td>BSS DIPLOMA IN FIRE & SAFETY ENGINEERING TECHNIQUES</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>FSS019</td>
                        <td>BSS DIPLOMA IN HEALTH, SAFETY & ENVIRONMENT MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>FSS020</td>
                        <td>ADVANCED DIPLOMA IN FIRE & INDUSTRIAL SAFETY MANAGEMENT</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>FSS021</td>
                        <td>FIRE MAN TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                       <td>21</td>
                        <td>FSS022</td>
                        <td>BSS DIPLOMA IN FIRE INDUSTRIAL SAFETY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>FSS023</td>
                        <td>DIPLOMA IN CONSTRUCTION SAFETY MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>FSS026</td>
                        <td>POST DIPLOMA IN FIRE & INDUSTRIAL SAFETY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>FSS027</td>
                        <td>BSS DIPLOMA IN INDUSTRIAL SAFETY ENGINEERING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>FSS028</td>
                        <td>BSS DIPLOMA IN INDUSTRIAL SAFETY AND DISASTER MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>FSS029</td>
                        <td>POST DIPLOMA IN FIRE AND INDUSTRIAL SAFETY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>27</td>
                        <td>FSS030</td>
                        <td>BSS DIPLOMA IN FIRE SAFETY ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>FSS031</td>
                        <td>BSS DIPLOMA IN FIRE AND CONSTRUCTION SAFETY MANAGEMENT</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>FSS032</td>
                        <td>BSS DIPLOMA IN FIRE AND INDUSTRIAL SAFETY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>FSS035</td>
                        <td>SUB FIRE STATION OFFICER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>31</td>
                        <td>FSS036</td>
                        <td>BSS DIPLOMA IN OCCUPATIONAL SAFETY & HEALTH</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>FSS037</td>
                        <td>CERTIFICATE IN OCCUPATIONAL SAFETY & HEALTH</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>FSS039</td>
                        <td>POST DIPLOMA IN PETRO CHEMICAL PROCESS SAFETY & ENGINEERING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>FSS040</td>
                        <td>ADVANCED DIPLOMA IN INDUSTRIAL SAFETY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>FSS041</td>
                        <td>BSS DIPLOMA IN INDUSTRIAL ENVIRONMENTAL SAFETY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>FSS042</td>
                        <td>BSS POST DIPLOMA IN HAZARD ANALYSIS CRITICAL CONTROL POINT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>37</td>
                        <td>FSS043</td>
                        <td>BSS POST DIPLOMA IN POWER PLANT ENGINEERING,SAFETY & TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>FSS044</td>
                        <td>BSS ADVANCED DIPLOMA IN OCCUPATIONAL HEALTH,SAFETY,ENVIRONMENT&RISK MGMT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>39</td>
                        <td>FSS046</td>
                        <td>ADVANCED DIPLOMA IN OFFSHORE,RIG,OIL AND GAS SAFETY ENGINEERING</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>FSS047</td>
                        <td>BSS ADVANCED DIPLOMA IN OIL & GAS SAFETY ENGINEERING</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>41</td>
                        <td>FSS048</td>
                        <td>BSS ADVANCED DIPLOMA IN OCCUPATIONAL SAFETY,HEALTH & ENVIRONMENT MGMT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>FSS049</td>
                        <td>POST DIPLOMA IN TRANSPORT OF HAZARDOUS AND DANGEROUS GOODS BY ROAD</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>FSS050</td>
                        <td>BSS DIPLOMA IN FIRE AND SAFETY ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>FSS051</td>
                        <td>CERTIFICATE COURSE IN FIRE ENGINEERING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>45</td>
                        <td>FSS052</td>
                        <td>DIPLOMA IN FIRE AND SAFETY ENGINEERING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>FSS053</td>
                        <td>BSS DIPLOMA IN FIRE ENGINEERING & SAFETY MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>47</td>
                        <td>FSS054</td>
                        <td>BSS DIPLOMA IN FIRE SAFETY & SECURITY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>FSS055</td>
                        <td>POST DIPLOMA IN FIRE,INDUSTRIAL & ENVIRONMENT HEALTH SAFETY MGMT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>49</td>
                        <td>FSS056</td>
                        <td>BSS ADVANCED DIPLOMA IN FIRE ENGINEERING& INDUSTRIAL SAFETY MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>    
                     <tr>
                     
                        <td>50</td>
                        <td>FSS057</td>
                        <td>BSS ADVANCED DIPLOMA IN CERTIFIED HEALTH SAFETY & ENVIRONMENT</td>
                          <td>ONE YEAR</td>
                    </tr>                    
                </tbody>
            </table>
           
                  <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #7: FOREST & ENVIRONMENTAL SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>FES001</td>
                        <td>BSS DIPLOMA IN AGRO – FOREST EDUCATION</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>FES002</td>
                        <td>BSS DIPLOMA IN RURAL MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>FES003</td>
                        <td>BSS DIPLOMA IN POLLUTION CONTROL MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>FES004</td>
                        <td>BSS DIPLOMA IN SUSTAINABLE ENVIRONMENTAL MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>FES005</td>
                        <td>BSS DIPLOMA IN SOIL CONSERVATION & RURAL CONSTRUCTION TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>FES006</td>
                        <td>BSS DIPLOMA IN ENVIRONMENTAL ADMINISTRATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>FES007</td>
                        <td>AGRO-FOREST PRODUCTS STORAGE TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>FES008</td>
                        <td>AGRO-FOREST PRODUCTS PACKING TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>FES009</td>
                        <td>AGRO-FOREST PRODUCTS QUALITY CONTROL TECHNICIAN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>FES011</td>
                        <td>AGRO-FOREST PRODUCTS PROCESSING TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>FES012</td>
                        <td>BSS DIPLOMA EROSION TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                  </tbody>
                </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #8: GEM & JEWELLERY SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>GJS001</td>
                        <td>JEWEL MANUFACTURING</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>GJS002</td>
                        <td>JEWEL CASTING TECHNIQUES</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>GJS003</td>
                        <td>GOLD REFINING & ASSAYING TECHNIQUES</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>GJS004</td>
                        <td>ELECTROPLATING & POLISHING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>GJS005</td>
                        <td>BSS CERTIFIED JEWELLERY PROFESSIONAL COURSE</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>GJS006</td>
                        <td>JEWEL DESIGNING - CAD</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>GJS007</td>
                        <td>GEMMOLOGY</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>GJS008</td>
                        <td>STONE SETTING (METAL SETTING)</td>
                        <td>ONE MONTH<</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>GJS009</td>
                        <td>JEWELLERY APPRAISER</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>GJS011</td>
                        <td>JEWELLERY SALES MAN / WOMAN</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>GJS011</td>
                        <td>DIAMOND GRADING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                     
                        <td>12</td>
                        <td>GJS012</td>
                        <td>JEWELLERY HOSTESS</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>GJS013</td>
                        <td>JEWELLERY APPRAISER</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                        
                        <td>14</td>
                        <td>GJS014</td>
                        <td>JEWELS & PRECIOUS METAL TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                  </tbody>
                </table>
                 <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #9: GEO SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>GS001</td>
                        <td>BSS DIPLOMA IN GEOGRAPHIC INFORMATION SYSTEM</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>GS002</td>
                        <td>BSS DIPLOMA IN GPS NAVIGATION SYSTEM</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>GS003</td>
                        <td>BSS DIPLOMA IN GEO-SATELITTE COMMUNICATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>GS004</td>
                        <td>BSS DIPLOMA IN GPS INSTRUMENT TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>GS005</td>
                        <td>BSS DIPLOMAIN REMOTE SENSING AND GIS TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                  </tbody>
                </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #10: HOMOEOPATHY SCHOOLHS0</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>HS001</td>
                        <td>BSS DIPLOMA IN HOMOEOPATHY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>HS002</td>
                        <td>BSS DIPLOMA IN HOMOEOPATHY & NATUROPATHY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>HS003</td>
                        <td>BSS DIPLOMA IN BIO-CHEMIC & FLOWER MEDICINE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>HS004</td>
                        <td>CERTIFICATE IN ELECTRO – HOMOEOPATHY</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>HS006</td>
                        <td>CERTIFICATE OF HOMOEO PUNCTURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>HS007</td>
                        <td>CERTIFICATE OF FLOWER MEDICINE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>HS009</td>
                        <td>CERTIFICATE OF DIABETIC CARE IN HOMOEOPATHY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>HS010</td>
                        <td>CERTIFICATE OF ARTHRITIC CARE IN HOMOEOPATHY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>HS011</td>
                        <td>BSS DIPLOMA IN HOMOEOPATHY NURSING</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>HS012</td>
                        <td>BSS DIPLOMA IN HOMOEOPATHY PHARMACY ASSISTANT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>HS014</td>
                        <td>BSS ADVANCED DIPLOMA IN HOMOEOPATHY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>12</td>
                        <td>HS015</td>
                        <td>BSS DIPLOMA IN HOMOEO & FLOWER REMEDIES</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>HS016</td>
                        <td>BSS ADVANCED DIPLOMA IN HOMOEOPATHY & FLOWER REMEDIES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>14</td>
                        <td>HS017</td>
                        <td>BSS DIPLOMA IN PSYCHOLOGICAL COUNSELING WITH FLOWER REMEDIES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>15</td>
                        <td>HS025</td>
                        <td>BSS DIPLOMA IN HOMAEOPATHIC PHARMACY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>16</td>
                        <td>HS026</td>
                        <td>BSS DIPLOMA IN HOMOEOPATHY SCIENCE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>HS027</td>
                        <td>DIPLOMA IN ELECTROPATHY SCIENCE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>18</td>
                        <td>HS028</td>
                        <td>POST DIPLOMA IN ELECTROPATHY SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                  </tbody>
                </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #11: HOTEL MANAGEMENT & TOURISM SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>HMTS001</td>
                        <td>BSS DIPLOMA IN HOTEL MANAGEMENT & TOURISM</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>HMTS002</td>
                        <td>BSS DIPLOMA IN HOTEL MANAGEMENT & CATERING SCIENCE</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>HMTS003</td>
                        <td>BSS DIPLOMA IN HOTEL MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>HMTS004</td>
                        <td>BSS DIPLOMA IN INSTITUTIONAL HOUSE KEEPING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>HMTS005</td>
                        <td>BSS DIPLOMA IN HOTEL OPERATION & MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>HMTS006</td>
                        <td>BSS DIPLOMA IN TOURISM ADMINISTRATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>HMTS007</td>
                        <td>BAKERY BAKING TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>HMTS008</td>
                        <td>TRAVEL COUNSELORS</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>HMTS009</td>
                        <td>PREPARATION OF TOMATO KETCHUP/PICKLES</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>HMTS010</td>
                        <td>BSS DIPLOMA IN FOOD PRODUCTION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>HMTS011</td>
                        <td>BSS DIPLOMA IN FOOD AND BEVERAGE SERVICE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>HMTS012</td>
                        <td>BSS DIPLOMA IN BAKERY AND CONFECTIONERY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>HMTS013</td>
                        <td>BSS DIPLOMA IN FRONT OFFICE MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>HMTS014</td>
                        <td>CANNING AND PRESERVATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>HMTS015</td>
                        <td>TOURIST GUIDE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>HMTS016</td>
                        <td>SNACKS PRESERVATION</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                        
                        <td>17</td>
                        <td>HMTS017</td>
                        <td>COUNTER SALE/RESERVATION ASSISTANT</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>HMTS018</td>
                        <td>VINEGAR MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>HMTS019</td>
                        <td>BELL CAPTION</td>
                       <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>HMTS020</td>
                        <td>SMALL HOTEL & MOTEL OPERATING TECHNIQUES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                       <td>21</td>
                        <td>HMTS021</td>
                        <td>VEGETABLE CARVING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>HMTS022</td>
                        <td>FISH FOOD PROCESSING</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>HMTS023</td>
                        <td>FRUIT PROCESSING (JAM, JELLY, JUICE)</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>HMTS024</td>
                        <td>ATTENDANTS IN STAR HOTEL, FLIGHT AND RAILWAY CATERING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>HMTS025</td>
                        <td>GENERAL RECEPTIONIST</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>HMTS026</td>
                        <td>TOURISM AND TRAVEL TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>27</td>
                        <td>HMTS027</td>
                        <td>FOOD AND BEVERAGE SERVICE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>HMTS028</td>
                        <td>BSS DIPLOMA IN CORPORATE HOUSE KEEPING</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>HMTS029</td>
                        <td>BSS DIPLOMA IN PROFESSIONAL COOKERY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>HMTS030</td>
                        <td>CATERING SCIENCES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>31</td>
                        <td>HMTS031</td>
                        <td>SNACKS PRODUCTION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>HMTS032</td>
                        <td>BSS DIPLOMA IN FOOD PRESERVATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>HMTS033</td>
                        <td>POST DIPLOMA IN HOSPITALITY MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>HMTS034</td>
                        <td>POST DIPLOMA IN HOTEL MANAGEMENT & CATERING SCIENCES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>HMTS035</td>
                        <td>BSS DIPLOMA IN FOOD TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>HMTS036</td>
                        <td>BSS DIPLOMA IN HOUSE KEEPING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>37</td>
                        <td>HMTS037</td>
                        <td>BSS DIPLOMA IN HOTEL & TOURISM MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>HMTS038</td>
                        <td>BSS DIPLOMA IN HOTEL & CATERING MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>39</td>
                        <td>HMTS039</td>
                        <td>BSS DIPLOMA IN CULINARY SKILLS</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>HMTS040</td>
                        <td>BSS DIPLOMA IN CATERING TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>41</td>
                        <td>HMTS041</td>
                        <td>BSS DIPLOMA IN DOMESTIC HOUSE KEEPING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>HMTS042</td>
                        <td>BSS ADVANCED DIPLOMA IN CULINARY SKILLS</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>HMTS045</td>
                        <td>BSS DIPLOMA IN CATERING TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>HMTS046</td>
                        <td>ADVANCED DIPLOMA IN CULINARY SKILLS</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>45</td>
                        <td>HMTS047</td>
                        <td>BSS DIPLOMA IN FOOD AND NUTRITION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>HMTS048</td>
                        <td>FOOD PROCESSING AND PRESERVATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>47</td>
                        <td>HMTS049</td>
                        <td>TOURISM AND TRAVEL AGENCY MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>HMTS050</td>
                        <td>BSS DIPLOMA IN HOSPITALITY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>49</td>
                        <td>HMTS051</td>
                        <td>CRAFTSMANSHIP IN BARTENDING</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>50</td>
                        <td>HMTS052</td>
                        <td>ADVANCED MIXOLOGY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>51</td>
                        <td>HMTS053</td>
                        <td>DIPLOMA IN MIXOLOGY & BARTENDING</td>
                        <td>ONE YEAR</td>
                    </tr>                    
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #12: LANGUAGE SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>LS001</td>
                        <td>CERTIFICATE IN ENGLISH</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>LS002</td>
                        <td>BSS DIPLOMA IN ENGLISH</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>LS003</td>
                        <td>BSS ADVANCED DIPLOMA IN ENGLISH</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>LS004</td>
                        <td>CERTIFICATE IN GERMAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>LS005</td>
                        <td>BSS DIPLOMA IN GERMAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>LS006</td>
                        <td>BSS ADVANCED DIPLOMA IN GERMAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>LS007</td>
                        <td>CERTIFICATE IN FRENCH</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>LS008</td>
                        <td>BSS DIPLOMA IN FRENCH</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>LS009</td>
                        <td>BSS ADVANCED DIPLOMA IN FRENCH</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>LS010</td>
                        <td>CERTIFICATE IN HINDI</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>LS011</td>
                        <td>BSS DIPLOMA IN HINDI</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>LS012</td>
                        <td>BSS ADVANCED DIPLOMA IN HINDI</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>LS016</td>
                        <td>BSS DIPLOMA IN URDU EDUCATION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>LS017</td>
                        <td>BSS TEACHING ENGLISH TO SPEAKER OF OTHER LANGUAGES</td>
                        <td>THREE MONTHS</td>
                    </tr>
                  </tbody>
                </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #13: MUSIC SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>MS001</td>
                        <td>BSS DIPLOMA IN VOCAL MUSIC</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>MS002</td>
                        <td>BSS ADVANCED DIPLOMA IN VOCAL MUSIC</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>MS003</td>
                        <td>BSS DIPLOMA IN CINE MUSIC DIRECTION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>MS004</td>
                        <td>COMPOSER IN ELECTRONIC MEDIA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>MS005</td>
                        <td>TEMPURA PLAYER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>MS006</td>
                        <td>BSS DIPLOMA IN FLUTE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>MS007</td>
                        <td>BSS ADVANCED DIPLOMA IN FLUTE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>MS008</td>
                        <td>BSS DIPLOMA IN KEY BOARD</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>MS009</td>
                        <td>BSS ADVANCED DIPLOMA IN KEY BOARD</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>MS010</td>
                        <td>BSS DIPLOMA IN VEENA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>MS011</td>
                        <td>BSS ADVANCED DIPLOMA IN VEENA</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>MS012</td>
                        <td>BSS DIPLOMA IN VIOLIN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>MS013</td>
                        <td>BSS ADVANCED DIPLOMA IN VIOLIN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>MS014</td>
                        <td>BSS DIPLOMA IN BHARATHANATIYAM</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>MS015</td>
                        <td>KEY BOARD PLAYER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>16</td>
                        <td>MS016</td>
                        <td>FLUTE PLAYER</td>
                         <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>MS019</td>
                        <td>BSS ADVANCED DIPLOMA IN BHARADHANATIYAM</td>
                        <td>SIX MONTHS</td>
                    </tr>
                  </tbody>
                </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #14: SIDDHA SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>SIS001</td>
                        <td>BSS DIPLOMA IN CLINICAL SIDDHA PRACTICES</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>SIS002</td>
                        <td>BSS DIPLOMA IN TRADITIONAL HEALTH PRACTICES</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>SIS003</td>
                        <td>BSS DIPLOMA IN TRADITIONAL SIDDHA</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>SIS004</td>
                        <td>BSS DIPLOMA IN SIDDHA & AYURVEDA</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>SIS005</td>
                        <td>BSS DIPLOMA IN HERBAL MEDICINES</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>SIS006</td>
                        <td>BSS DIPLOMA IN SIDDHA SCIENCE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>SIS007</td>
                        <td>CERTIFICATE IN SIDDHA FOOD PRODUCTION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>SIS008</td>
                        <td>CERTIFICATE IN SKIN CARE IN SIDDHA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>SIS009</td>
                        <td>CERTIFICATE IN HAIR FALL MANAGEMENT BY HERBALS</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>SIS010</td>
                        <td>CERTIFICATE IN INFERTILITY TREATING BY HERBOLOGY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>SIS011</td>
                        <td>CERTIFICATE COURSE IN SIDDHA PATIENT CARE ASSISTANT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>SIS012</td>
                        <td>BSS DIPLOMA IN SIDDHA SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>SIS014</td>
                        <td>BSS DIPLOMA IN TRADITIONAL HEALTH SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>SIS015</td>
                        <td>BSS ADVANCED DIPLOMA IN SIDDHA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>SIS016</td>
                        <td>BSS DIPLOMA IN SIDDHA DIETOTHERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>16</td>
                        <td>SIS017</td>
                        <td>BSS DIPLOMA IN ELECTRO MAGNETISM & HERBAL REMEDIES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>SIS018</td>
                        <td>BSS DIPLOMA IN HERBAL SIDDHA MEDICINE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>SIS019</td>
                        <td>BSS DIPLOMA IN BIO ELECTRO MAGNETISM & HERBAL REMEDIES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>SIS020</td>
                        <td>BSS ADVANCED DIPLOMA IN VARMA KALAI AND THOKKANAM</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>SIS021</td>
                        <td>DIPLOMA IN SIDDHA PRACTIES AND VARMA</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>SIS022</td>
                        <td>BSS DIPLOMA IN NUTRITION AND HERBAL REMEDIES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>22</td>
                        <td>SIS023</td>
                        <td>BSS ADVANCED DIPLOMA IN TRADITIONAL HEALTH PRACTICES</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>23</td>
                        <td>SIS024</td>
                        <td>DIPLOMA IN VARMA PRACTICES</td>
                         <td>TWO YEARS</td>
                    </tr>
                  </tbody>
                </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #15: SPORTS SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>SSS001</td>
                        <td>BSS DIPLOMA IN ADVENTURE SPORTS FOR TRAINERS</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>SSS002</td>
                        <td>BSS DIPLOMA IN SPORTS MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>SSS003</td>
                        <td>CERTIFICATE IN ADVENTURE SPORTS</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>SSS004</td>
                        <td>BSS DIPLOMA IN PHYSICAL EDUCATION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>SSS005</td>
                        <td>BSS DIPLOMA IN SPORTS INSTRUMENTS TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>SSS006</td>
                        <td>SPORTS INSTRUMENT TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>SSS007</td>
                        <td>BSS ADVANCED DIPLOMA IN SPORTS MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>SSS008</td>
                        <td>BSS ADVANCED DIPLOMA IN PHYSICAL EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>SSS009</td>
                        <td>BSS ADVANCED DIPLOMA IN ADVENTURE SPORTS FOR TRAINERS</td>
                          <td>ONE YEAR</td>
                    </tr>
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #16: TECHNICAL TRAINING SCHOOLTS0</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>TTS001</td>
                        <td>BSS DIPLOMA IN GENERAL MACHINIST</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>TTS002</td>
                        <td>BSS DIPLOMA IN ADVANCE INSTRUMENT TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>TTS003</td>
                        <td>BSS DIPLOMA IN CALL CENTER TECHNICIAN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>TTS004</td>
                        <td>BSS DIPLOMA IN JEWELS & PRECIOUS METAL TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>TTS005</td>
                        <td>BSS DIPLOMA IN JIGS & FIXTURE MAKER</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>TTS006</td>
                        <td>BSS DIPLOMA IN LENS & PRISM GRINDING TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>TTS007</td>
                        <td>BSS DIPLOMA IN PIPE AND PRESSURE VESSELS TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>TTS008</td>
                        <td>BSS DIPLOMA IN STRUCTURAL WELDING TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>TTS009</td>
                        <td>WELDING TECHNICIAN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>10</td>
                        <td>TTS010</td>
                        <td>BSS DIPLOMA IN TIG AND MIG WELDING TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>TTS011</td>
                        <td>BSS DIPLOMA IN INDUSTRIAL AUTOMATION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>TTS012</td>
                        <td>BSS DIPLOMA IN CARPENTRY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>TTS013</td>
                        <td>INDUSTRIAL INSTRUMENT TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>TTS014</td>
                        <td>INSTRUMENT TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>TTS015</td>
                        <td>MECHANICAL FITTER</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>TTS016</td>
                        <td>BSS DIPLOMA IN PLUMBING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>17</td>
                        <td>TTS017</td>
                        <td>BSS DIPLOMA IN CHEMICAL LAB TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>TTS018</td>
                        <td>OIL MILLS PROCESSING TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>TTS019</td>
                        <td>FOOD GRAIN MILL PROCESSING TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>20</td>
                        <td>TTS020</td>
                        <td>TIE & DYE TECHNICIAN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>21</td>
                        <td>TTS021</td>
                        <td>BSS DIPLOMA IN OIL & GAS SAFETY MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>TTS022</td>
                        <td>BSS DIPLOMA IN ADVANCED DIESEL TECHNOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>TTS023</td>
                        <td>X RAY WELDING</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>TTS024</td>
                        <td>BSS DIPLOMA IN PRINTING TECHNOLOGY2</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>TTS025</td>
                        <td>BSS DIPLOMA IN INSTRUCTOR TRAINING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>TTS026</td>
                        <td>BSS DIPLOMA IN HOLLOW BLOCK MAKING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>27</td>
                        <td>TTS027</td>
                        <td>ELECTRICAL WIRING TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>TTS028</td>
                        <td>BSS DIPLOMA IN SCREEN PRINTING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>TTS029</td>
                        <td>BSS DIPLOMA IN TEXTILE MECHATRONICS</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>30</td>
                        <td>TTS030</td>
                        <td>BSS DIPLOMA IN EMBEDED SYSTEMS & PLC TECHNICIAN</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>31</td>
                        <td>TTS032</td>
                        <td>GAS & ARC WELDER</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>TTS034</td>
                        <td>DIESEL MECHANISM (STATIONERY & MOBILE ENGINES)</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>TTS036</td>
                        <td>BSS DIPLOMA IN ELECTRICAL WIRING & ELECTRONICS</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>TTS037</td>
                        <td>BSS DIPLOMA IN MECHANICAL ENGINEERING TECHNIQUES</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>TTS038</td>
                        <td>BSS ADVANCED DIPLOMA IN TOOL & DIEMAKING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>TTS039</td>
                        <td>BSS DIPLOMA IN TOOL & DIE MAKING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>37</td>
                        <td>TTS040</td>
                        <td>BSS DIPLOMA IN PROCESS CONTROL & INSTRUMENTATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>TTS041</td>
                        <td>BSS DIPLOMA IN WELDING TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>39</td>
                        <td>TTS042</td>
                        <td>BSS DIPLOMA IN MASSIONERY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>40</td>
                        <td>TTS043</td>
                        <td>BSS DIPLOMA IN MECHANICAL FITTER</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>41</td>
                        <td>TTS045</td>
                        <td>DIPLOMA IN AUTO MECHANIC</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>TTS047</td>
                        <td>BSS DIPLOMA IN SOLAR SYSTEM & INVERTOR INSTALLATION & SERVICING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>TTS050</td>
                        <td>BSS DIPLOMA IN SENIOR SOLAR PHOTOVOLTAIC TECHNICIAN</td>
                          <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>TTS051</td>
                        <td>BSS DIPLOMA IN JUNIOR SOLAR PHOTOVOLTAIC TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>45</td>
                        <td>TTS052</td>
                        <td>BSS DIPLOMA IN SENIOR SOLAR PHOTOVOLTAIC PANET ASSEMBLING TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>TTS053</td>
                        <td>BSS DIPLOMA IN SENIOR SOLAR PCU TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>47</td>
                        <td>TTS054</td>
                        <td>BSS DIPLOMA IN SOLAR PLANT & ELECTRICAL TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>TTS056</td>
                        <td>BSS DIPLOMA IN ATTENDANT OPERATOR OF CHEMICAL PLAN</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>49</td>
                        <td>TTS057</td>
                        <td>BSS DIPLOMA IN FIBER OPTIC TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>50</td>
                        <td>TTS058</td>
                        <td>BSS DIPLOMA IN EARTH MOVING EQUIPMENT REPAIRING</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>51</td>
                        <td>TTS059</td>
                        <td>DEALERS SERVICE MECHANIC</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>52</td>
                        <td>TTS063</td>
                        <td>BSS DIPLOMA IN PRODUCTION TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>53</td>
                        <td>TTS064</td>
                        <td>BSS DIPLOMA IN PRODUCTION TECHNIQUES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>54</td>
                        <td>TTS065</td>
                        <td>BSS DIPLOMA IN LEATHER AND FOOTWEAR TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>55</td>
                        <td>TTS067</td>
                        <td>DIPLOMA IN ELEVATOR TECHNOLOGY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>56</td>
                        <td>TTS068</td>
                        <td>BSS DIPLOMA IN MEP ENGINEERING FACILITY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>57</td>
                        <td>TTS069</td>
                        <td>BSS DIPLOMA IN POWER PLANT OPERATION AND MAINTENANCE ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>58</td>
                        <td>TTS070</td>
                        <td>TURNER</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>59</td>
                        <td>TTS071</td>
                        <td>MATERIAL TESTING & MACHINE OPERATOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>60</td>
                        <td>TTS072</td>
                        <td>MECHANIC AUTO BODY PAINTING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>61</td>
                        <td>TTS073</td>
                        <td>MECHANIC AUTO BODY REPAIR</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>62</td>
                        <td>TTS074</td>
                        <td>BSS DIPLOMA IN SOLAR PHOTOVOLTAIC ENGINEERING ASSISTANT</td>
                          <td>THREE MONTHS</td>
                    </tr>
                   
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #17: TEXTILE SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>TS001</td>
                        <td>BBSS DIPLOMA IN TEXTILE TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>TS002</td>
                        <td>BSS DIPLOMA IN PRINTING ON CLOTHES</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>TS003</td>
                        <td>BSS DIPLOMA IN TEXTILE MACHINERY TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>TS004</td>
                        <td>TEXTILE LAB TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>TS005</td>
                        <td>BSS DIPLOMA IN TEXTILE LAB TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>TS006</td>
                        <td>CERTIFICATE IN YARN MAKING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    </tbody>
                </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #18: UNANI SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>US001</td>
                        <td>BSS DIPLOMA IN UNANI SCIENCE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>US002</td>
                        <td>BSS DIPLOMA IN UNANI & ACUPUNCTURE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>US003</td>
                        <td>BSS DIPLOMA IN UNANI FOR CHRONIC AILMENTS</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>US004</td>
                        <td>BSS DIPLOMA IN UNANI FOR KIDNEY DISEASES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>US005</td>
                        <td>CERTIFICATE OF BASIC UNANI</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>US006</td>
                        <td>CERTIFICATE OF ADVANCE UNANI</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>7</td>
                        <td>US008</td>
                        <td>BSS ADVANCED DIPLOMA IN UNANI</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>US009</td>
                        <td>BSS DIPLOMA IN UNANI PHARMACY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>9</td>
                        <td>US010</td>
                        <td>BSS ADVANCED DIPLOMA IN UNANI SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>TS010</td>
                        <td>DIPLOMA IN HIJAMA THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    </tbody>
                </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #19: YOGA AND NATUROPATHY SCHOOL</h3>
            <hr class="space m" />
            <table class="table">        
                <thead>
               <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>YNS001</td>
                        <td>BSS DIPLOMA IN YOGA & NATUROPATHY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>YNS002</td>
                        <td>BSS DIPLOMA IN NATUROPATHY & YOGIC SCIENCE</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>YNS003</td>
                        <td>BSS DIPLOMA IN YOGA & MASSAGE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>YNS004</td>
                        <td>BSS DIPLOMA IN VARMA AND YOGA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>YNS005</td>
                        <td>BSS DIPLOMA IN NATUROPATHY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>YNS006</td>
                        <td>BSS DIPLOMA IN HERBAL DRUG MANUFACTURING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>7</td>
                        <td>YNS007</td>
                        <td>CERTIFICATE IN PRACTICAL YOGA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>YNS008</td>
                        <td>CERTIFICATE IN NATURE CURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>YNS009</td>
                        <td>CERTIFICATE IN YOGIC SCIENCE</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>10</td>
                        <td>YNS010</td>
                        <td>CERTIFICATE IN PRANIC HEALING AND YOGA</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>11</td>
                        <td>YNS011</td>
                        <td>CERTIFICATE IN WATER THERAPY AND NATURAL DIET</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>YNS012</td>
                        <td>CERTIFICATE IN REIKI THERAPY AND NATURE SCIENCE</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>YNS013</td>
                        <td>CERTIFICATE IN ARTHRITIC CARE & NATURAL DIET</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>YNS014</td>
                        <td>CERTIFICATE IN PULSE DIAGNOSING IN NATURAL SCIENCE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>15</td>
                        <td>YNS015</td>
                        <td>CERTIFICATE IN COLOR THERAPY AND NATURE CURE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>YNS017</td>
                        <td>CERTIFICATE IN GEM THERAPY AND YOGA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>17</td>
                        <td>YNS018</td>
                        <td>CERTIFICATE IN CHAKRAS AND NATURAL SCIENCE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>YNS019</td>
                        <td>CERTIFICATE IN HYPNOTHERAPY AND YOGA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>YNS020</td>
                        <td>CERTIFICATE IN HERBAL NUTRITION BASICS AND YOGA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>20</td>
                        <td>YNS021</td>
                        <td>CERTIFICATE IN NATURAL VITAMINS & MINERALS</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>21</td>
                        <td>YNS022</td>
                        <td>CERTIFICATE IN METABOLISM & NUTRITION BY NATURAL DIET</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>YNS023</td>
                        <td>CERTIFICATE IN ENERGY THERAPY AND YOGA</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>YNS024</td>
                        <td>CERTIFICATE IN HERBS & NATURAL DIET</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>YNS025</td>
                        <td>CERTIFICATE IN SPINAL MANIPULATION BY NATUROPATHY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>YNS026</td>
                        <td>CERTIFICATE IN CHILD HEALTH BY YOGA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>YNS027</td>
                        <td>CERTIFICATE IN BREATHING TECHNIQUES BY YOGA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        <td>27</td>
                        <td>YNS028</td>
                        <td>CERTIFICATE IN MIND MEMORY PERSONALITY DEVELOPMENT BY YOGA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>YNS029</td>
                        <td>CERTIFICATE IN SCIENCE OF HAPPY AND HEALTHY LIVING BY NATURAL DIET</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>YNS030</td>
                        <td>CERTIFICATE IN PYRAMID THERAPY AND YOGA</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>30</td>
                        <td>YNS031</td>
                        <td>CERTIFICATE IN VASTU SHASTRA AND NATURE SCIENCE</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>31</td>
                        <td>YNS032</td>
                        <td>CERTIFICATE IN FASTING AND NATUROPATHY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>YNS033</td>
                        <td>CERTIFICATE IN HYDRO THERAPY AND YOGA</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>YNS034</td>
                        <td>CERTIFICATE IN PILLARS OF HEALTH AND NATURAL SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>YNS035</td>
                        <td>CERTIFICATE IN THERAPEUTIC YOGA</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>YNS036</td>
                        <td>CERTIFICATE IN NATUROPARHY FOOD</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>YNS037</td>
                        <td>BSS DIPLOMA IN YOGA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>37</td>
                        <td>YNS038</td>
                        <td>BSS DIPLOMA IN BODY BUILDING (HEALTH) FITNESS, AND NUTRITION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>YNS039</td>
                        <td>BSS ADVANCED DIPLOMA IN YOGA & NATUROPATHIC SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>39</td>
                        <td>YNS040</td>
                        <td>BSS DIPLOMA IN YOGA & ACUPUNCTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>40</td>
                        <td>YNS041</td>
                        <td>BSS DIPLOMA IN YOGA & NATURE CARE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>41</td>
                        <td>YNS042</td>
                        <td>BSS POST DIPLOMA IN HOLISTIC THERAPY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>YNS043</td>
                        <td>BSS DIPLOMA IN INTEGRATIVE ALTERNATIVE THERAPIES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>YNS044</td>
                        <td>BSS DIPLOMA IN YOGA & NATUROPATHY SCIENCE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>YNS045</td>
                        <td>BSS DIPLOMA IN DRUG STORE MANAGEMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>45</td>
                        <td>YNS046</td>
                        <td>BSS DIPLOMA IN NATUROPATHY & YOGIC SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>YNS047</td>
                        <td>BSS DIPLOMA IN YOGA TEACHER EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>47</td>
                        <td>YNS048</td>
                        <td>BSS ADVANCED DIPLOMA IN YOGA & NATUROPATHY SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>YNS050</td>
                        <td>ENERGY HEALING & TRAINING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>49</td>
                        <td>YNS051</td>
                        <td>BSS ADVANCED DIPLOMA IN OZONE THERAPY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>50</td>
                        <td>YNS052</td>
                        <td>BSS DIPLOMA IN HYPNOTHERAPY</td>
                          <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>51</td>
                        <td>YNS053</td>
                        <td>DIPLOMA IN QUANTUM MAGNETIC HEALTH ANALYSER & HEALTH ADVISOR</td>
                          <td>ONE YEAR</td>
                    </tr>                    
                    <tr>                      
                        <td>52</td>
                        <td>YNS054</td>
                        <td>BSS DIPLOMA IN INTUTIVE MIND EMPOWERMENT COURSE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>53</td>
                        <td>YNS055</td>
                        <td>BSS DIPLOMA IN MAGNETIC BIO HEALTH ANALYZER</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>54</td>
                        <td>YNS056</td>
                        <td>BSS DIPLOMA IN BIO-PLASMIC MODULATION THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>55</td>
                        <td>YNS057</td>
                        <td>BSS DIPLOMA IN CUPPING THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>56</td>
                        <td>YNS058</td>
                        <td>BSS ADVANCED DIPLOMA IN YOGA AND NATUROPATHY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>57</td>
                        <td>YNS059</td>
                        <td>BSS DIPLOMA IN MEDITATION, YOGA AND TANTRA</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>58</td>
                        <td>YNS060</td>
                        <td>BSS CERTIFICATE COURSE IN HYPNOSIS</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>59</td>
                        <td>YNS061</td>
                        <td>BSS POST DIPLOMA IN HYPNOTHERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>60</td>
                        <td>YNS062</td>
                        <td>BSS CERTIFICATE COURSE IN PAST LIFE REGRESSION</td>
                          <td>THREE MONTHS</td>
                    </tr>                                     
                </tbody>
            </table>
          </div>
    </div>

  