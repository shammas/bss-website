
     <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">               
                <h1>About Us</h1>
            </div>
        </div>
    </div>
    <div id="about-page">
    <div class="section-empty">
        <div class="container content">
            <div class="row proporzional-row">
                <div class="col-md-4  col-sm-6 boxed white middle-content">
                  <img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" />  
                </div>
                <div class="col-md-8 col-sm-6">
                    <hr class="space m visible-sm" />
                    <hr class="space" />
            
                    <p>
                      Bharat Sevak Samaj is National Development Agency,Established in 1952 by Planning Commission,Government of India to ensure public co-operation for implementing Government plans. The main purpose behind the formulation of Bharat Sevak Samaj is to initiate a nation wide,non official and non political organisation with the object of enabling individual citizen to contribute,in the form of an organized co-operative effort,to the implementation of the National Government Plan. The constitution and functioning of Bharat Sevak Samaj is approved unanimously by the Indian Parliament.  
                    </p>
                   
                </div>
            </div>
            <hr class="space" />            
            <div class="row proporzional-row">
                <div class="col-md-8 col-sm-12">
                    <p class="text-color">
                     JAWAHARLAL NEHRU's Message
                    </p>
                    <p>
                    "very difficult to measure the biggest of these resources that we have,that is man-power, and that psychology which makes people work for greater ends,Unless we utilize this man-power and unless we can produce that temper in our people which laughs at difficulties and gets things done,sometimes in spite of facts,we cannot achieve anything really big.
                    </p>
                    <p>
                     The proposal to start an organization to be called the Bharat Sevak Samaj, has this in view, It is an ambitious task and we want men and  women with high ambitions for it, not the ambition for little and personal thing of life but the ambition to serve great causes, forgetting on self and achieve great ends.   
                    </p>
                    <p>
                    This is not a political organization, even though, I, a politician, command it. It is meant for every able bodies men and women, whatever his or her views might be on other topics."</p>
                    <hr class="space xs" />
                    <a href="about-bss" target="_blank" class="btn btn-sm"></i>Read more about BSS</a>
                </div>
                <div class="col-md-4 col-sm-12">
                    <hr class="space visible-sm" />
                    <div class="flexslider slider nav-inner white" data-options="controlNav:true,directionNav:true">
                        <ul class="slides">
                            <li>
                                <a class="img-box lightbox" href="">
                                    <img src="<?php echo base_url();?>assets/images/about.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="img-box lightbox" href="">
                                    <img src="<?php echo base_url();?>assets/images/about2.jpg" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
