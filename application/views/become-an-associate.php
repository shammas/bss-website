
     <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>Become an Associate</h1>
            </div>
        </div>
    </div>

   <div class="container content container-xs">
            <div class="row">
                <div class="col-md-12">          
                   <hr class="space s" />
                    <h2 class="aligncenter text-color">Application for associate</h2>
                    <p class="aligncenter">
                        Even individuals can associate with Bharat Sevak Samaj to participate in various National Development Activities, spread various skills among the citizens, create employability etc.
If you are a trainer or coach in any segment, you can associate with BSS to issue Participation/Skill Certificate to your trainees.
You can get the status of “Bharat Sevak”, this is a honoring program by recognizing your social activities or achievements. 
If you want to become a BSS Associate/Bharat Sevak, apply Now:

                    </p>
                    
                    <hr class="space s" />
                    <!-- <form action="#" > -->
                      <?php echo form_open_multipart('become_associate_application', ['id' => 'form','name' => 'contactForm']);?>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Full Name</p>
                                <input id="name" name="name" placeholder="" type="text" class="form-control form-value" required>
                            </div>
                          </div>
                          <hr class="space s" />
                             <div class="row">
                            <div class="col-md-6">
                               <p>Age</p>
                                <input id="age" name="age" placeholder="" type="number" class="form-control form-value" required>  
                            </div>
                            
                        <div class="col-md-6">
                              <p>Sex</p>
                              <input id="sex" name="sex" placeholder="" type="text" class="form-control form-value" required>
                        </div>
                      </div>
                      <hr class="space s" />
                             <div class="row">
                            <div class="col-md-6">
                               <p>Mobile No</p>
                                <input id="mobileno" name="mobileno" placeholder="" type="tel" class="form-control form-value" required>  
                            </div>
                            <div class="col-md-6">
                               <p>WhatsApp No</p>
                                <input id="whatsappno" name="whatsappno" placeholder="" type="tel" class="form-control form-value" required>  
                            </div>
                          </div>

                       <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>Email</p>
                                <input id="email" name="email" placeholder="" type="email" class="form-control form-value" required>
                               
                            </div>
                        </div>
                      <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>Address</p>
                                <input id="address" name="address" placeholder="" type="text" class="form-control form-value" required>                               
                            </div>
                        </div>
                         <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>  Qualification</p>
                                <input id="education" name="education" placeholder="" type="text" class="form-control form-value" required>                               
                            </div>
                        </div>
                     
                        <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p> Your Training /Coaching Field </p>
                                 <input id="course" name="course" placeholder="" type="text" class="form-control form-value" required> 
                               
                            </div>
                        </div>
                        <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                                <p>Message</p>
                                <textarea id="message" name="message" class="form-control form-value" required></textarea>
                                <hr class="space s" />
                                <button class="anima-button btn-border btn-sm btn" type="submit"><i class="fa fa-mail-reply-all"></i>Submit Application</button>
                            </div>
                        </div>
                       
                    </form>
                </div>
               
            </div>
        </div>
 