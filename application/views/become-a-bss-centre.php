
    <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>Become a bss centre</h1>
            </div>
        </div>
    </div>
    <div class="container content container-xs">
            <div class="row">
                <div class="col-md-12">          
                   <hr class="space s" />
                    <h2 class="aligncenter text-color">Application for Centre Affiliation</h2>
                    <p class="aligncenter">
                        You can open an Authorised Training Centre at your place in minimum investment. The process is very simple. Apply now:
                    </p>
                    
                    <hr class="space s" />
                    <!-- <form action="#"> -->
                      <?php echo form_open_multipart('centre_affiliation_application', ['id' => 'form','name' => 'contactForm']);?>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Full Name</p>
                                <input id="name" name="name" placeholder="" type="text" class="form-control form-value" required>
                            </div>
                          </div>
                          <hr class="space s" />
                             <div class="row">
                            <div class="col-md-6">
                               <p>Age</p>
                                <input id="age" name="age" placeholder="" type="number" class="form-control form-value" required>  
                            </div>
                            
                        <div class="col-md-6">
                              <p>Sex</p>
                              <input id="sex" name="sex" placeholder="" type="text" class="form-control form-value" required>
                        </div>
                      </div>
                      <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>Place</p>
                                <input id="place" name="place" placeholder="" type="text" class="form-control form-value" required>                               
                            </div>
                        </div>
                         <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>   Educational Qualification</p>
                                <input id="education" name="education" placeholder="" type="text" class="form-control form-value" required>                               
                            </div>
                        </div>
                     <hr class="space s" />
                             <div class="row">
                            <div class="col-md-6">
                               <p>Mobile No</p>
                                <input id="mobileno" name="mobileno" placeholder="" type="tel" class="form-control form-value" required>  
                            </div>
                            <div class="col-md-6">
                               <p>WhatsApp No</p>
                                <input id="whatsappno" name="whatsappno" placeholder="" type="tel" class="form-control form-value" required>  
                            </div>
                          </div>
                       <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p>Email</p>
                                <input id="email" name="email" placeholder="" type="email" class="form-control form-value" required>
                               
                            </div>
                        </div>
                        <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                             <p> Name of Courses you want conduct </p>
                                 <input id="course" name="course" placeholder="" type="text" class="form-control form-value"  required  autocomplete="off"> 
                               
                            </div>
                        </div>
                        <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                                <p>Message</p>
                                <textarea id="message" name="message" class="form-control form-value" required></textarea>
                                <hr class="space s" />
                                <button class="anima-button btn-border btn-sm btn" type="submit"><i class="fa fa-mail-reply-all"></i>Submit Application</button>
                            </div>
                        </div>
                       
                    </form>
                </div>
               
            </div>
        </div>

 