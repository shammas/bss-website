
    <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1> Courses </h1>
            </div>
        </div>
    </div>
   <div id="courses_page">
      
         <div class="section-empty section-item">
        <div class="container content">
            <div class="maso-list">
                
                <div class="maso-box row">
                    <div  class="maso-item col-md-4 col-sm-6">
                        <div class="advs-box advs-box-multiple boxed-inverse" >
                            <a class="img-box" href="medical-technology-courses" ><img  src="<?php echo base_url();?>assets/images/medical.jpg" alt="" /></a>
                         
                            <div class="advs-box-content">
                               <a href="medical-technology-courses"> <h4>MEDICAL TECHNOLOGY COURSES</h4>
                                <p>
                                    BSS have many Paramedical Courses under Medical Technology Courses. You can start exclusive academy to conduct these courses.
                                </p></a>
                                <a class="btn-text" href="medical-technology-courses">Know More </a>
                            </div>
                        </div>
                    </div>
                     <div  class="maso-item col-md-4 col-sm-6">
                        <div class="advs-box advs-box-multiple boxed-inverse" >
                            <a class="img-box" href="category-courses"><img  src="<?php echo base_url();?>assets/images/bg22.jpg" alt="" /></a>
                            
                            <div class="advs-box-content">
                               <a href="category-courses"> <h4>CATEGORY COURSES</h4>
                                <p>
                                    There are hundreds of courses from different category and you can conduct different courses from different categories.
                                </p></a>
                                <a class="btn-text" href="category-courses">Know More </a>
                            </div>
                        </div>
                    </div>
                     <div  class="maso-item col-md-4 col-sm-6">
                        <div class="advs-box advs-box-multiple boxed-inverse" >
                            <a class="img-box" href="school-courses" ><img  src="<?php echo base_url();?>assets/images/230.jpg" alt="" /></a>
                         
                            <div class="advs-box-content">
                                <a href="school-courses"><h4>SCHOOL COURSES</h4>
                                <p>
                                    BSS have different School Courses. You can conduct some or all courses from a particular School.
                                </p></a>
                                 <a class="btn-text" href="school-courses">Know More </a>
                            </div>
                        </div>
                    </div>
                     <div  class="maso-item col-md-4 col-sm-6">
                        <div class="advs-box advs-box-multiple boxed-inverse" >
                            <a class="img-box" href="special-courses" ><img  src="<?php echo base_url();?>assets/images/avi.jpg" alt="" /></a>
                          
                            <div class="advs-box-content">
                              <a href="special-courses">  <h4>SPECIAL COURSES</h4>
                                <p>
                                    Aviation, Bio-Technology, Business, Fashion, Media and Shipping Courses are coming under Special Courses.
                                </p></a>
                                 <a class="btn-text" href="special-courses">Know More </a>
                            </div>
                        </div>
                    </div>
                     <div  class="maso-item col-md-4 col-sm-6">
                        <div class="advs-box advs-box-multiple boxed-inverse" >
                            <a class="img-box" href="it-courses" ><img  src="<?php echo base_url();?>assets/images/itcourses.jpg" alt="" /></a>
                           
                            <div class="advs-box-content">
                               <a href="it-courses"> <h4>IT COURSES </h4>
                                <p>
                                    BSS have Government/PSC recognised and industry recognised Information Technology Courses.
                                </p></a>
                                 <a class="btn-text" href="it-courses">Know More </a>
                            </div>
                        </div>
                    </div>
                   
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    </div>
 