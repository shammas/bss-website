
     <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>Gallery</h1>
            </div>
        </div>
    </div>
    <div id="gallerypage">
      <div class="section-empty section-item">
        <div class="container content">           
              <div class="grid-list gallery">
                <div class="grid-box row" data-lightbox-anima="show-scale">
                    <?php
                    if (isset($galleries) and $galleries) {
                        foreach ($galleries as $gallery) {
                        ?>
                        <div class="grid-item col-md-4">
                            <a class="img-box i-center" href="<?php echo $gallery->url . $gallery->file_name;?>" data-anima="show-scale" data-trigger="hover" data-anima-out="hide">
                                <i class="fa fa-camera anima"></i>
                                <img   alt="<?php echo $gallery->name;?>" src="<?php echo $gallery->url . $gallery->file_name;?>" >
                            </a>
                        </div>
                         <?php 
                        }
                    }
                    ?>    
                </div>

                <div class="list-nav">
                    <ul class="pagination-sm pagination-grid" data-page-items="12" data-pagination-anima="show-scale"></ul>
                </div>
               
            </div>
</div>
</div>
</div>
 