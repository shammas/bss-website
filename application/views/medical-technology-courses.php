
    <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>Medical Technology Courses</h1>
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
        <div class="container content">           
            <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #1: ALLIED HEALTH EDUCATION  see also ALLIED HEALTH SCHOOL</h3>
            <hr class="space m" />
            <table class="table">            
                <thead>
                 <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>AHE001</td>
                        <td>BSS DIPLOMA IN MEDICAL LABORATORY TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>AHE002</td>
                        <td>MEDICAL LABORATORY TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>AHE003</td>
                        <td>BSS DIPLOMA IN X-RAY TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>AHE004</td>
                        <td>X-RAY TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>5</td>
                        <td>AHE005</td>
                        <td>BSS DIPLOMA IN OPERATION THEATRE TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>AHE006</td>
                        <td>OPERATION THEATRE TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>7</td>
                        <td>AHE007</td>
                        <td>LAB ASSISTANT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>AHE008</td>
                        <td>E C G TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>AHE009</td>
                        <td>CT SCAN TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>AHE010</td>
                        <td>MRI SCAN TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>11</td>
                        <td>AHE011</td>
                        <td>EYE TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>AHE012</td>
                        <td>DENTAL TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>AHE014</td>
                        <td>BSS DIPLOMA IN MEDICAL TRANSCRIPTION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>AHE016</td>
                        <td>BSS DIPLOMA IN OPTOMETRY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>15</td>
                        <td>AHE017</td>
                        <td>CERTIFICATE IN DIABETOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>AHE019</td>
                        <td>CERTIFICATE IN OPTHALMIC ASSISTANT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>17</td>
                        <td>AHE020</td>
                        <td>ORTHO & TRAUMA CARE TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>AHE021</td>
                        <td>DENTAL LAB TECHNICIAN</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>AHE024</td>
                        <td>BSS DIPLOMA IN TRAUMA CARE & CASUALTY TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>AHE025</td>
                        <td>BSS DIPLOMA IN DIALYSIS TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                      <tr>
                        
                        <td>21</td>
                        <td>AHE026</td>
                        <td>MEDICAL EQUIPMENT TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>AHE028</td>
                        <td>BSS DIPLOMA IN PALLIATIVE CARE THERAPIST</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>AHE029</td>
                        <td>CERTIFICATE IN DIALYSIS TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>AHE030</td>
                        <td>BSS DIPLOMA IN ANESTHESIA TECHNOLOGY </td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>25</td>
                        <td>AHE031</td>
                        <td>BSS DIPLOMA IN MEDICAL EQUIPMENT TECHNOLOGY </td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>AHE032</td>
                        <td>BSS DIPLOMA IN ACCIDENT & EMERGENCY TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>27</td>
                        <td>AHE033</td>
                        <td>EMERGENCY PATIENT CARE TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>AHE034</td>
                        <td>BSS POST DIPLOMA IN PATIENT CARE MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>AHE035</td>
                        <td>PHYSIOTHERAPY TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>AHE038</td>
                        <td>POST DIPLOMA IN SANITARY HEALTH INSPECTOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>31</td>
                        <td>AHE040</td>
                        <td>BSS DIPLOMA IN PHYSICIAN ASSISTANT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>AHE041</td>
                        <td>BSS DIPLOMA IN CRITICAL CARE MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>AHE042</td>
                        <td>ICU TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>AHE043</td>
                        <td>CERTIFICATE IN DIETICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>35</td>
                        <td>AHE049</td>
                        <td>DENTAL HYGIENIST</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>AHE052</td>
                        <td>BSS DIPLOMA IN GENERAL HEALTHCARE & MATERNITY ASSISTANT</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>37</td>
                        <td>AHE053</td>
                        <td>AUDIO-METRY TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>AHE055</td>
                        <td>BSS DIPLOMA IN LAPROSCOPY ASSISTANT</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>39</td>
                        <td>AHE062</td>
                        <td>BSS DIPLOMA IN OPTOMETRY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>AHE063</td>
                        <td>BSS DIPLOMA IN ANESTHESIA TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
               
                    <tr>
                        
                        <td>41</td>
                        <td>AHE064</td>
                        <td>BSS DIPLOMA IN OPTHALMIC ASSISTANCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>AHE065</td>
                        <td>BSS DIPLOMA IN OPTHALMIC ASSISTANT</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>AHE066</td>
                        <td>BSS DIPLOMA IN MEDICAL RECORD SCIENCES</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>AHE070</td>
                        <td>PHARMACY PROFESSION & COMMUNITY HEALTH</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                        
                        <td>45</td>
                        <td>AHE071</td>
                        <td>BSS DIPLOMA IN CVTS TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>AHE074</td>
                        <td>BSS DIPLOMA IN E C G TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>47</td>
                        <td>AHE075</td>
                        <td>BSS DIPLOMA IN EEG & EMG TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>AHE076</td>
                        <td>BSS DIPLOMA IN VISION CARE TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>49</td>
                        <td>AHE077</td>
                        <td>BSS DIPLOMA IN ULTRA SONOGRAPHY TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>50</td>
                        <td>AHE078</td>
                        <td>BSS DIPLOMA IN X RAY & IMAGING TECHNOLOG</td>
                        <td>TWO YEARS</td>
                    </tr>
                      <tr>
                        
                        <td>51</td>
                        <td>AHE082</td>
                        <td>BSS DIPLOMA IN DENTAL ASSISTANT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>52</td>
                        <td>AHE083</td>
                        <td>BSS DIPLOMA IN DENTAL ASSISTAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>53</td>
                        <td>AHE086</td>
                        <td>ADVANCED DIPLOMA IN PHYSICIAN ASSISTANT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>54</td>
                        <td>AHE087</td>
                        <td>ADVANCED DIPLOMA IN FORENSIC ODONTOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>55</td>
                        <td>AHE088</td>
                        <td>BSS DIPLOMA IN RURAL MEDICAL CARE PROVIDER</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>56</td>
                        <td>AHE089</td>
                        <td>BSS DIPLOMA IN OCCUPATIONAL HEALTH</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>57</td>
                        <td>AHE090</td>
                        <td>BSS ADVANCED DIPLOMA IN MEDICAL LABORATORY TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>58</td>
                        <td>AHE091</td>
                        <td>BSS ADVANCED DIPLOMA IN OPERATION THEATRE TECHNOLOGY</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>59</td>
                        <td>AHE092</td>
                        <td>BSS DIPLOMA IN SPECIAL EDUCATION (DEAF BLINDNESS)</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>60</td>
                        <td>AHE093</td>
                        <td>BSS ADVANCED DIPLOMA IN HEARING IMPAIRMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>61</td>
                        <td>AHE094</td>
                        <td>BSS DIPLOMA IN MENTAL RETRADATION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>62</td>
                        <td>AHE095</td>
                        <td>BSS DIPLOMA IN LEARNING DISABILITY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>63</td>
                        <td>AHE096</td>
                        <td>BSS DIPLOMA IN EARLY CHILDHOOD SPECIAL EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>64</td>
                        <td>AHE097</td>
                        <td>BSS ADVANCED DIPLOMA IN LEARNING DISABILITY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>65</td>
                        <td>AHE098</td>
                        <td>BSS DIPLOMA IN AUDIOLOGY AND SPEECH - LANGUAGE PATHOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>66</td>
                        <td>AHE099</td>
                        <td>BSS ADVANCED DIPLOMA IN AUDIOLOGY AND SPEECH-LANGUAGE PATHOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>67</td>
                        <td>AHE100</td>
                        <td>BSS ADVANCED DIPLOMA IN REHABILITATION PSYCHOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>68</td>
                        <td>AHE101</td>
                        <td>ADVANCED DIPLOMA IN MEDICAL PSYCHOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>69</td>
                        <td>AHE102</td>
                        <td>BSS ADVANCED DIPLOMA IN CLINICAL LABORATORY TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>70</td>
                        <td>AHE103</td>
                        <td>BSS DIPLOMA IN PSYCHOLOGICAL GUIDANCE & COUNSELING</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>71</td>
                        <td>AHE106</td>
                        <td>BSS DIPLOMA IN BURNS MANAGEMENT AND SKIN BANKING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>72</td>
                        <td>AHE107</td>
                        <td>BSS DIPLOMA IN AUTISM SPECTRUM DISORDER EDUCATION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                </tbody>
            </table>
                <hr class="space m" />
             <h3 class=" text-center text-color">SCHOOL COURSES #1: ALLIED HEALTH SCHOOL            see also ALLIED HEALTH EDUCATION</h3>
            <hr class="space m" />
            <table class="table">            
                <thead>
                 <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>AHS001</td>
                        <td>BSS DIPLOMA IN PATIENT CARE</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>AHS002</td>
                        <td>PATIENT CARE</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>AHS003</td>
                        <td>BSS DIPLOMA IN PHYSIOTHERAPY & ACTIVITY THERAPY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>AHS004</td>
                        <td>PHYSIOTHERAPY & ACTIVITY THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>5</td>
                        <td>AHS005</td>
                        <td>BSS DIPLOMA IN PATIENT CARE ASSISTANT</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>AHS006</td>
                        <td>PATIENT CARE ASSISTANT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>7</td>
                        <td>AHS007</td>
                        <td>BSS DIPLOMA IN FIRST AID AND PATIENT CARE</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>AHS008</td>
                        <td>FIRST AID AND PATIENT CARE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>9</td>
                        <td>AHS009</td>
                        <td>HOSPITAL DOCUMENTATION AND RECORD KEEPING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>10</td>
                        <td>AHS010</td>
                        <td>PHARMACY ASSISTANT</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>11</td>
                        <td>AHS011</td>
                        <td>BSS DIPLOMA IN HEALTH INSPECTOR</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>AHS012</td>
                        <td>VILLAGE HEALTH WORKER</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>13</td>
                        <td>AHS013</td>
                        <td>BSS DIPLOMA IN SANITARY HEALTH INSPECTOR</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>14</td>
                        <td>AHS014</td>
                        <td>BSS DIPLOMA IN HOSPITAL MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                        
                        <td>15</td>
                        <td>AHS015</td>
                        <td>BSS DIPLOMA IN HOSPITAL DOCUMENTATION & RECORDS MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>AHS016</td>
                        <td>BSS DIPLOMA IN MULTI PURPOSE HEALTH WORKER</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>17</td>
                        <td>AHS017</td>
                        <td>CERTIFICATE IN AWARENESS OF HIV & AIDS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>AHS018</td>
                        <td>CERTIFICATE IN BLOOD BANK ASSISTANT</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>19</td>
                        <td>AHS019</td>
                        <td>CERTIFICATE IN HOSPITAL HOUSE KEEPING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>AHS020</td>
                        <td>BSS DIPLOMA IN HOSPITAL WASTE MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                      <tr>
                        
                        <td>21</td>
                        <td>AHS021</td>
                        <td>CERTIFICATE IN OLD AGE CARE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>AHS022</td>
                        <td>CERTIFICATE IN FEMALE PATIENT CARE ASSISTANT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>23</td>
                        <td>AHS023</td>
                        <td>BSS DIPLOMA IN PATIENT CARE ADMINISTRATION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>24</td>
                        <td>AHS024</td>
                        <td>CERTIFICATE IN PATIENT CARE & DEVELOPMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>25</td>
                        <td>AHS031</td>
                        <td>BSS DIPLOMA IN C.S.S.D TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>26</td>
                        <td>AHS026</td>
                        <td>BSS DIPLOMA IN HEALTH CARE PROFESSIONAL TECHNICIAN</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>27</td>
                        <td>AHS027</td>
                        <td>BSS DIPLOMA IN HEALTH CARE ASSISTANT</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>28</td>
                        <td>AHS028</td>
                        <td>BSS DIPLOMA IN HOSPITAL DRESSING TECHNIQUES</td>
                        <td>ONE YEAS</td>
                    </tr>
                    <tr>
                     
                        <td>29</td>
                        <td>AHS029</td>
                        <td>BSS DIPLOMA IN C.S.S.D TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>AHS030</td>
                        <td>BSS DIPLOMA IN HOSPITAL FRONT OFFICE MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>31</td>
                        <td>AHS031</td>
                        <td>BSS DIPLOMA IN FEMALE PATIENT CARE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>AHS032</td>
                        <td>BSS DIPLOMA IN FEMALE PATIENT CARE ASSISTANT</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>33</td>
                        <td>AHS033</td>
                        <td>BSS ADVANCED DIPLOMA IN CELL THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>AHS034</td>
                        <td>BSS ADVANCED DIPLOMA IN PANCHGAVYA THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>35</td>
                        <td>AHS035</td>
                        <td>BSS DIPLOMA IN PANCHGAVYA THERAPY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>AHS036</td>
                        <td>BSS DIPLOMA IN SANITARY HEALTH INSPECTOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>37</td>
                        <td>AHS037</td>
                        <td>CERTIFICATE IN ANTI-MALARIA TRAINING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>AHS041</td>
                        <td>BSS DIPLOMA IN COMMUNITY HEALTH</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>39</td>
                        <td>AHS042</td>
                        <td>CERTIFICATE IN OPTICAL TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>AHS044</td>
                        <td>BSS DIPLOMA IN FEG AND EMG TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
               
                    <tr>
                        
                        <td>41</td>
                        <td>AHS046</td>
                        <td>CERTIFICATE COURSE IN DIET & NUTRITION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>AHE047</td>
                        <td>BSS DIPLOMA IN PATIENT CARE & DEVELOPMENT</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>43</td>
                        <td>AHS048</td>
                        <td>BSS DIPLOMA IN FEMALE HEALTH WORKER</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>44</td>
                        <td>AHS049</td>
                        <td>BSS DIPLOMA IN HOSPITAL ADMINISTRATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                        
                        <td>45</td>
                        <td>AHS050</td>
                        <td>CERTIFICATION COURSE FOR HOSPITAL MANAGERS</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>AHS051</td>
                        <td>BSS POST DIPLOMA IN MEDICAL COSMETOLOGY</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>47</td>
                        <td>AHS052</td>
                        <td>BSS DIPLOMA IN HOSPITAL INFECTION CONTROL & PREVENTION</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>48</td>
                        <td>AHS053</td>
                        <td>CERTIFICATE IN HOSPITAL ASSISTANT & TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>49</td>
                        <td>AHS054</td>
                        <td>BSS DIPLOMA IN HOSPITAL & HEALTH CARE MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>50</td>
                        <td>AHS058</td>
                        <td>BSS DIPLOMA IN MULTIPURPOSE HEALTH WORKER</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>51</td>
                        <td>AHS058</td>
                        <td>CERTIFICATE IN HEALTH CARE ASSISTANT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>52</td>
                        <td>AHS060</td>
                        <td>CERTIFICATE COURSE IN COMMUNITY HEALTH WORKER</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>53</td>
                        <td>AHS061</td>
                        <td>BSS ADVANCED DIPLOMA IN PATIENT CARE</td>
                        <td>ONE YEAR</td>
                    </tr>
                      <tr>
                        
                        <td>54</td>
                        <td>AHS062</td>
                        <td>BSS DIPLOMA IN GENERAL PATIENT CARE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>55</td>
                        <td>AHS063</td>
                        <td>CERTIFICATE IN APPLIED MICROBIOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                </tbody>
            </table>
          </div>
    </div>

 