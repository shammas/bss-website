

   <div class="header-title" style="background-image:url(<?php echo base_url();?>assets/images/bg-61.jpg);">
        <div class="container">
            <div class="title-base">
                <h1>Category Courses</h1>
            </div>
        </div>
    </div> 
     <div class="section-empty section-item">
        <div class="container content">
            <hr class="space m" />
            <h3 class=" text-center text-color">CATEGORY COURSES #2: AGRICULTURE EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>AE001</td>
                        <td>BSS DIPLOMA IN AGRICULTURE AND ALLIED TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>AE002</td>
                        <td>BSS DIPLOMA IN HORTICULTURE</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>AE003</td>
                        <td>BSS DIPLOMA IN COLD STORAGE TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>AE004</td>
                        <td>BEE KEEPING EQUIPMENT MANUFACTURING TECHNIQUES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>AE005</td>
                        <td>FARM EQUIPMENT MECHANIC</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>AE006</td>
                        <td>BSS DIPLOMA IN PLANT PROTECTION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>AE007</td>
                        <td>TRACTOR AND POWER TILLER MECHANIC</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>AE008</td>
                        <td>BSS DIPLOMA IN MEDICINAL PLANTS PROCESSING</td>
                        <td>TWO YEARS</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>AE009</td>
                        <td>COCONUT- BY PRODUCTS PROCESSING TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>AE010</td>
                        <td>HORTICULTURE SALESMAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>11</td>
                        <td>AE011</td>
                        <td>PRESERVATION TECHNICIAN (HORTICULTURAL)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>AE012</td>
                        <td>LABORATORY TECHNICIAN (HORTICULTURAL)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>AE013</td>
                        <td>FLORICULTURE TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>AE014</td>
                        <td>FLOWERS AND PLANTS PACKAGING TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>AE015</td>
                        <td>LAND SPACING TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>AE016</td>
                        <td>FLORIST SALESMAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>AE017</td>
                        <td>AGRICULTURE SUPERVISOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>AE018</td>
                        <td>BSS DIPLOMA IN AGRICULTURE SCIENCE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>19</td>
                        <td>AE019</td>
                        <td>PLANT PROTECTION TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>AE020</td>
                        <td>BSS DIPLOMA IN VOCATIONAL INSTRUCTOR (CROP PRODUCTION)</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>AE021</td>
                        <td>BSS DIPLOMA IN AGRICULTURE</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>AE022</td>
                        <td>BSS DIPLOMA IN AGRO-BASED INDUSTRIAL MANAGEMENT</td>
                        <td>TWO YEARS</td>
                    </tr>
                     <tr>
                      
                        <td>23</td>
                        <td>AE023</td>
                        <td>PLANT NURSERY MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                     <tr>
                      
                        <td>24</td>
                        <td>AE024</td>
                        <td>WATER SHED PROTECTION AND MANAGEMENT</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>AE025</td>
                        <td>FODDER CROPS CULTIVATION</td>
                        <td>ONE MONTH</td>
                    </tr>
                     <tr>
                      
                        <td>26</td>
                        <td>AE026</td>
                        <td>BSS DIPLOMA IN CROPS PRODUCTION</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>27</td>
                        <td>AE027</td>
                        <td>MUSHROOM CULTIVATION AND PROTECTION</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>28</td>
                        <td>AE028</td>
                        <td>HONEY BEE KEEPING TECHNICIAN</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>29</td>
                        <td>AE029</td>
                        <td>RAIN WATER HARVESTING TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>AE030</td>
                        <td>SOIL CONSERVATION TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>31</td>
                        <td>AE031</td>
                        <td>VERMICULTURE TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>AE032</td>
                        <td>IRRIGATION EQUIPMENT MECHANIC</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>33</td>
                        <td>AE033</td>
                        <td>VANILLA CULTIVATION PLANT PROTECTION</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>AE034</td>
                        <td>BSS DIPLOMA IN MEDICINAL PLANT CULTIVATION TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>AE035</td>
                        <td>HONEY PROCESSING TECHNICIAN</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>AE036</td>
                        <td>AZZOLA CULTIVATION TECHNICIAN</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>37</td>
                        <td>AE037</td>
                        <td>BSS DIPLOMA IN FOOD PRESERVATION TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>AE038</td>
                        <td>SEED PRODUCTION TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>39</td>
                        <td>AE039</td>
                        <td>BSS DIPLOMA IN FOOD TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>AE040</td>
                        <td>APIARY TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>41</td>
                        <td>AE041</td>
                        <td>CERTIFICATE IN AGRICULTURE SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>AE042</td>
                        <td>CERTIFICATE COURSE IN AGRICULTURE CROP PRODUCTION</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>43</td>
                        <td>AE043</td>
                        <td>BSS DIPLOMA IN PLANT PROTECTION AND PESTICIDES MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                </tbody>
            </table>
           
            <hr class="space m" />
            <h3 class=" text-center text-color">CATEGORY COURSES #3: AUTOMOBILE EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>AME001</td>
                        <td>BSS DIPLOMA IN AUTOMOBILE TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>AME002</td>
                        <td>BSS DIPLOMA IN HEAVY MOTOR VEHICLE MECHANISM</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>AME003</td>
                        <td>FOUR WHEELER MECHANIC (LMV)</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>AME004</td>
                        <td>BSS DIPLOMA IN AUTOMOBILE ELECTRONICS TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>AME005</td>
                        <td>CAR A/C MECHANIC</td>
                       <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>AME006</td>
                        <td>SPRAY PAINTER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>AME007</td>
                        <td>PLANT MAINTENANCE TECHNICIAN</td>
                       <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>AME008</td>
                        <td>JUNIOR PLANT OPERATOR</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>AME009</td>
                        <td>TWO WHEELER MECHANIC</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>AME010</td>
                        <td>BACKHOE LOADER OPERATION & MAINTENANCE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>11</td>
                        <td>AME011</td>
                        <td>DIESEL FUEL SYSTEM SERVICE MECHANIC</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>AME012</td>
                        <td>CRANE OPERATION & MAINTENANCE</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>AME013</td>
                        <td>FORK LIFT OPERATION & MAINTENANCE</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>AME014</td>
                        <td>BSS DIPLOMA IN INSPECTOR OF AUTO MANUFACTURING INDUSTRY</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>AME015</td>
                        <td>HYDRAULIC EXCAVATOR OPERATION & MAINTENANCE</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>AME016</td>
                        <td>BSS DIPLOMA IN ADVANCED DIESEL ENGINE TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>AME017</td>
                        <td>VEHICLE SURVEYOR</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>AME018</td>
                        <td>BSS CERTIFICATE IN AUTOMOBILE ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>19</td>
                        <td>AME019</td>
                        <td>HEAVY CRANE OPERATION & MANAGEMENT</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>AME020</td>
                        <td>BSS DIPLOMA IN ADVANCED PETROL ENGINE TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>AME021</td>
                        <td>BSS DIPLOMA IN CNC PROGRAMMER</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>AME022</td>
                        <td>GAS WELDER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>23</td>
                        <td>AME023</td>
                        <td>CNC MACHINE OPERATION (TURNING & MILLING)</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>24</td>
                        <td>AME024</td>
                        <td>LIGHT MOTOR VEHICLE MECHANISM</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>AME025</td>
                        <td>DIESEL MECHANISM (STATIONARY & MOBILE ENGINES)</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>26</td>
                        <td>AME026</td>
                        <td>FORK LIFT DRIVING CUM MAINTENANCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>27</td>
                        <td>AME027</td>
                        <td>SHEET METAL WORKS AND TINKERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>28</td>
                        <td>AME028</td>
                        <td>BSS DIPLOMA IN AUTOMOBILE ENGINEERING TECHNIQUES</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>29</td>
                        <td>AME029</td>
                        <td>BSS DIPLOMA IN FOUR WHEELER MECHANISM</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>AME030</td>
                        <td>OPERATION & MAINTENANCE OF HYDRAULIC MACHINES</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>31</td>
                        <td>AME031</td>
                        <td>OPERATION & MAINTENANCE OF HEAVY MACHINES (BACK HOE LOADER)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>AME032</td>
                        <td>OPERATION & MAINTENANCE OF HEAVY MACHINES (HYDRAULIC EXCAVATOR)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>33</td>
                        <td>AME033</td>
                        <td>OPERATION & MAINTENANCE OF HEAVY MACHINES (CRANE)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>AME034</td>
                        <td>OPERATION & MAINTENANCE OF HEAVY MACHINES (FORK LIFT)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>AME035</td>
                        <td>POST DIPLOMA IN AUTOMOBILE ENGINEERING TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>AME036</td>
                        <td>BSS DIPLOMA IN AUTOMOBILE ENGINEERING TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>37</td>
                        <td>AME037</td>
                        <td>BSS DIPLOMA IN AUTOMOBILE ENGINEERING</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>AME038</td>
                        <td>CERTIFICATE COURSE IN MECHANICAL FITTRE CUM CNC MACHINE OPERATOR</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>39</td>
                        <td>AME039</td>
                        <td>BSS DIPLOMA IN HYDRAULIC MECHANICAL ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                 
                </tbody>
            </table>
             <hr class="space m" />
            <h3 class=" text-center text-color">CATEGORY COURSES #4: CHILD EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>CE001</td>
                        <td>BSS DIPLOMA IN PRIMARY EDUCATION</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>CE002</td>
                        <td>BSS DIPLOMA IN PRE-PRIMARY EDUCATION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>CE003</td>
                        <td>BSS DIPLOMA IN NURSERY EDUCATION</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>CE004</td>
                        <td>BSS DIPLOMA IN MONTESSORI AND CHILD EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>CE005</td>
                        <td>BSS DIPLOMA IN CRECHE AND PRE-SCHOOL MANAGEMENT</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>CE006</td>
                        <td>BSS ADVANCED DIPLOMA IN MONTESSORI AND CHILD EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>CE007</td>
                        <td>BSS DIPLOMA IN ANGANWADI WORKER</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>CE008</td>
                        <td>BSS DIPLOMA IN ANGANWADI SUPERVISOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>CE009</td>
                        <td>BSS DIPLOMA IN EARLY CHILDHOOD CARE AND EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>CE010</td>
                        <td>SISULEKSHMY NURSERY EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>11</td>
                        <td>CE011</td>
                        <td>ADVANCED DIPLOMA IN MONTESSORI,KINDERGARTEN & NURSERY EDUCATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>CE012</td>
                        <td>BSS DIPLOMA IN CHILD REMEDIAL EDUCATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>CE013</td>
                        <td>BSS DIPLOMA IN ART AND CRAFT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>CE014</td>
                        <td>BSS ADVANCED DIPLOMA IN MONTESSORI AND CHILD EDUCATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>CE015</td>
                        <td>DIPLOMA IN PRE-SCHOOL EDUCATION (DPSE)</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>CE016</td>
                        <td>DIPLOMA IN PRE-SCHOOL AND PRIMARY EDUCATION (DPPE)</td>
                        <td>TEN MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>CE017</td>
                        <td>CERTIFICATION PROGRAM IN TECHNOLOGY IN TEACHING (CPIT)</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>CE018</td>
                        <td>CERTIFICATE PROGRAM IN CLASSROOM TEACHING INSTRUCTIONAL STRATEGIES</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>19</td>
                        <td>CE019</td>
                        <td>CERTIFICATE PROGRAM IN COMMUNICATION SKILLS AND ENGLISH LANGUAGE</td>
                        <td>TWO MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>CE020</td>
                        <td>DIPLOMA IN EARLY EDUCATION SYSTEM</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>CE021</td>
                        <td>BSS ADVANCED DIPLOMA IN MONTESSORI AND PRIMARY EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>CE022</td>
                        <td>DIPLOMA IN ELEMENTARY EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>23</td>
                        <td>CE023</td>
                        <td>ADVANCED DIPLOMA IN ELEMENTARY EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>24</td>
                        <td>CE024</td>
                        <td>BSS ADVANCED DIPLOMA IN NURSERY AND PRIMARY EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>CE025</td>
                        <td>BSS ADVANCED DIPLOMA IN MONTESSORI AND GRADE LEVEL EDUCATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                     
                </tbody>
            </table>
             <hr class="space m" />
            <h3 class=" text-center text-color">CATEGORY COURSES #5: CIVIL & ARCHITECTURAL EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>CAE001</td>
                        <td>BSS DIPLOMA IN ARCHITECTURAL TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>CAE002</td>
                        <td>BSS DIPLOMA IN CIVIL CONSTRUCTION SUPERVISOR</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>CAE003</td>
                        <td>BSS DIPLOMA IN CIVIL DRAUGHTS MAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>CAE004</td>
                        <td>BSS DIPLOMA IN BUILDING SUPERVISOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>CAE005</td>
                        <td>LAND SURVEY ASSISTANT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>CAE006</td>
                        <td>BSS DIPLOMA IN BUILDING DESIGN</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>CAE007</td>
                        <td>BSS DIPLOMA IN LAND SURVEY & DOCUMENTATION</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>CAE008</td>
                        <td>BSS DIPLOMA IN CIVIL ENGINEERING TECHNIQUES</td>
                        <td>TWO YEARS</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>CAE009</td>
                        <td>STRUCTURAL TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>CAE010</td>
                        <td>BSS DIPLOMA IN BUILDING QUALITY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>11</td>
                        <td>CAE011</td>
                        <td>BSS DIPLOMA IN LAND SURVEY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>CAE012</td>
                        <td>POST DIPLOMA IN CIVIL ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>CAE014</td>
                        <td>LAND SURVEYING USING TOTAL STATION AND GPS</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>CAE016</td>
                        <td>QUANTITY SURVEYING & BILLING ENGINEER IN CIVIL CONSTRUCTION</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>CAE017</td>
                        <td>POST DIPLOMA IN LAND SURVEY ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>CAE018</td>
                        <td>CONSTRUCTION MANAGEMENT</td>
                        <td>THREE MONTHS</td>
                    </tr>
                </tbody>
            </table>
             <hr class="space m" />
            <h3 class=" text-center text-color">CATEGORY COURSES #6: COMMUNICATIVE & SOFT SKILLS EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>CSSE001</td>
                        <td>SPOKEN ENGLISH</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>CSSE002</td>
                        <td>BSS DIPLOMA IN CIVIL CONSTRUCTION SUPERVISOR</td>
                         <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>CSSE003</td>
                        <td>BSS DIPLOMA IN COMMUNICATION SKILLS DEVELOPMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>CSSE004</td>
                        <td>BSS DIPLOMA IN ENTREPRENEURSHIP SKILLS DEVELOPMENT</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>CSSE005</td>
                        <td>BSS DIPLOMA IN PERSONALITY TRAINER</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>CSSE006</td>
                        <td>BSS DIPLOMA IN COMMUNICATION SKILLS TRAINER</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>CSSE007</td>
                        <td>BSS DIPLOMA IN ENTREPRENEURSHIP TRAINER</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>CSSE008</td>
                        <td>BSS DIPLOMA IN EMPLOYABILITY SKILLS</td>
                         <td>ONE YEAR</td>
                    </tr>
                </tbody>
            </table>
            <hr class="space m" />
            <h3 class=" text-center text-color">CATEGORY COURSES #7: DAIRY EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>DE001</td>
                        <td>BSS DIPLOMA IN DAIRY TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>DE002</td>
                        <td>BSS DIPLOMA IN DAIRY PRODUCTS PROCESSING</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>DE003</td>
                        <td>DAIRY DOCK TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>DE004</td>
                        <td>MILK CHILLING CENTRE TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>DE005</td>
                        <td>DAIRY LABORATORY TECHNICIAN</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>DE006</td>
                        <td>DAIRY TECHNICIAN</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>DE007</td>
                        <td>DAIRY STORE TECHNICIAN</td>
                       <td>SIX MONTHS</td>
                    </tr>
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #8: ELECTRICAL & ELECTRONICS EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>EEE001</td>
                        <td>BSS DIPLOMA IN ELECTRICAL & ELECTRONICS TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>EEE002</td>
                        <td>BSS DIPLOMA IN ELECTRONICS & COMMUNICATION TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>EEE003</td>
                        <td>MOBILE PHONE TECHNICIAN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>EEE004</td>
                        <td>BSS DIPLOMA IN RADIO AND TELEVISION SERVICE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>EEE005</td>
                        <td>DOMESTIC ELECTRICAL MOTOR WINDER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>EEE006</td>
                        <td>BSS DIPLOMA IN INDUSTRIAL ELECTRONICS TECHNOLOGY</td>
                        <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>EEE007</td>
                        <td>POWER INVERTOR TECHNICIAN</td>
                         <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>EEE008</td>
                        <td>BSS DIPLOMA IN CONSUMER ELECTRONICS MAINTENANCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>EEE009</td>
                        <td>ELECTRICIAN</td>
                       <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>EEE010</td>
                        <td>HEAVY ELECTRICAL MOTOR WINDER</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>11</td>
                        <td>EEE011</td>
                        <td>BSS DIPLOMA IN AIR CONDITIONING AND REFRIGERATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>EEE012</td>
                        <td>BSS DIPLOMA IN ADVANCED MOBILE PHONE TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>EEE013</td>
                        <td>CLOCK & WATCH REPAIR TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>EEE014</td>
                        <td>TELEPHONE TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>EEE015</td>
                        <td>GAS STOVE TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>EEE016</td>
                        <td>EMERGENCY LIGHT TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>EEE017</td>
                        <td>SEWING MACHINE TECHNICIAN</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>EEE018</td>
                        <td>RADIO & TV ASSEMBLER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>19</td>
                        <td>EEE019</td>
                        <td>BSS DIPLOMA IN DOMESTIC ELECTRONIC EQUIPMENT SERVICING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>EEE020</td>
                        <td>CABLE TV NETWORK TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>EEE021</td>
                        <td>STABILIZER MAKER</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>EEE022</td>
                        <td>BSS DIPLOMA IN INVERTOR, UPS TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>23</td>
                        <td>EEE023</td>
                        <td>CABLE TV OPERATOR</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>24</td>
                        <td>EEE024</td>
                        <td>BSS DIPLOMA IN DTH TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>EEE025</td>
                        <td>ELECTRONIC CHOCK MAKER</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>26</td>
                        <td>EEE026</td>
                        <td>ELECTRONICS TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>27</td>
                        <td>EEE027</td>
                        <td>EMBEDDED SYSTEM & PLC TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>28</td>
                        <td>EEE028</td>
                        <td>ELECTRICAL POWER DRIVES TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>29</td>
                        <td>EEE029</td>
                        <td>BSS DIPLOMA IN ELECTRICAL MOTOR REWINDING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>EEE030</td>
                        <td>CERTIFICATE IN INDUSTRIAL ELECTRONICS</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>31</td>
                        <td>EEE031</td>
                        <td>CERTIFICATE IN TV MECHANISM</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>32</td>
                        <td>EEE032</td>
                        <td>BSS DIPLOMA IN H.V.A.C. TECHNICIAN</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>33</td>
                        <td>EEE033</td>
                        <td>SHEET METAL & FABRICATION</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>34</td>
                        <td>EEE034</td>
                        <td>BSS DIPLOMA IN GENERATOR OPERATION & SERVICE TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>35</td>
                        <td>EEE035</td>
                        <td>AIR CONDITIONING & REFRIGERATION MECHANISM</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>36</td>
                        <td>EEE036</td>
                        <td>BSS DIPLOMA IN REFRIGERATION & AIR CONDITIONING ENGINEERING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>37</td>
                        <td>EEE037</td>
                        <td>BSS DIPLOMA IN CCTV TECHNOLOGY & SURVEILLANCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>38</td>
                        <td>EEE038</td>
                        <td>CCTV TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>39</td>
                        <td>EEE039</td>
                        <td>BSS DIPLOMA IN REFRIGERATION ,HEAT VENTILATION AND AIR CONDITION EGG</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>40</td>
                        <td>EEE040</td>
                        <td>BSS DIPLOMA IN HEAT VENTILATION AND AIR CONDITION ENGINEERING</td>
                       <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>41</td>
                        <td>EEE041</td>
                        <td>CERTIFICATION IN PDM-ULTRASOUND</td>
                         <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>42</td>
                        <td>EEE042</td>
                        <td>CERTIFICATION IN PREDICTIVE MAINTENANCE</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>43</td>
                        <td>EEE043</td>
                        <td>CERTIFICATION IN ELECTRICAL QC</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>44</td>
                        <td>EEE044</td>
                        <td>CERTIFICATION IN PDM-INFRARED THERMOGRAPHY</td>
                         <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>45</td>
                        <td>EEE046</td>
                        <td>CERTIFICATION IN PDM-VIBRATION ANALYSIS</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>46</td>
                        <td>EEE046</td>
                        <td>CERTIFICATION IN CONCRETE NDT</td>
                        <td>THREE MONTHS</td>
                    </tr>
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #9: FISHERIES EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>1</td>
                        <td>FE001</td>
                        <td>BSS DIPLOMA IN FISHERIES TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>2</td>
                        <td>FE002</td>
                        <td>FISHERIES LABORATORY TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                     
                        <td>3</td>
                        <td>FE003</td>
                        <td>FISHERIES SEED PRODUCTION TECHNICIAN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>FE004</td>
                        <td>FISHERIES FEED TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>FE005</td>
                        <td>HATCHERY OPERATOR</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>FE006</td>
                        <td>NET AND GEAR TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>FE007</td>
                        <td>FISHERIES FIELD TECHNICIAN</td>
                         <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>FE008</td>
                        <td>FISHERIES SURVEY TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>FE009</td>
                        <td>BSS DIPLOMA IN COLD STORAGE TECHNOLOGY</td>
                       <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>FE010</td>
                        <td>FISH FARMER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>11</td>
                        <td>FE011</td>
                        <td>FISH BANK TECHNIQUES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>FE012</td>
                        <td>PITUITARY BANK TECHNIQUES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>FE013</td>
                        <td>ORNAMENTAL FISH CULTIVATION AND MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>FE014</td>
                        <td>BSS DIPLOMA IN BOAT CONSTRUCTION TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>FE015</td>
                        <td>SKILLED FRP WORKER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>FE016</td>
                        <td>SPAWN PRODUCING TECHNIQUES</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>FE017</td>
                        <td>FISHERIES FEED MANUFACTURING TECHNIQUES</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    </tbody>
                </table>
              <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #10: HOME BUSINESS EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead> 
                 <tbody>
              <tr>
                        
                        <td>1</td>
                        <td>HBE001</td>
                        <td>BOOK BINDING</td>
                          <td>ONE MONTH</td>
                    </tr>
                     <tr>
                        
                        <td>2</td>
                        <td>HBE002</td>
                        <td>TERRACOTTA WORK</td>
                          <td>ONE MONTH</td>
                    </tr>
                     <tr>
                        
                        <td>3</td>
                        <td>HBE003</td>
                        <td>WIRE BAG KNITTING</td>
                          <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>HBE004</td>
                        <td>FANCY ARTICLES MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>HBE005</td>
                        <td>SOFT TOYS MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>HBE006</td>
                        <td>DOLL MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>HBE007</td>
                        <td>ALUMINUM FOIL WORK</td>
                         <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>HBE008</td>
                        <td>JUTE WORK/CRAFT</td>
                        <td>ONE MONTH</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>HBE009</td>
                        <td>CANOPY WORK</td>
                       <td>ONE MONTH</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>HBE010</td>
                        <td>CLOTH BAG MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                     <tr>
                      
                        <td>11</td>
                        <td>HBE011</td>
                        <td>TOYS (PLASTER OF PARIS)</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>HBE012</td>
                        <td>COIR ITEMS MAKING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>HBE013</td>
                        <td>MAT MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>HBE014</td>
                        <td>UMBRELLA ASSEMBLING</td>
                        <td>ONE MONTH</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>HBE015</td>
                        <td>PAPER COVERS MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>HBE016</td>
                        <td>WAX DECORATIVE ARTICLES MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>HBE017</td>
                        <td>GREETINGS CARD MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>HBE018</td>
                        <td>IMPROVED CHULA MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>19</td>
                        <td>HBE019</td>
                        <td>FILE AND ENVELOPE MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>HBE020</td>
                        <td>CHALK PIECES AND CANDLE MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>HBE021</td>
                        <td>HAIR BAND MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>HBE022</td>
                        <td>RUBBER STAMPS MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                     <tr>
                      
                        <td>23</td>
                        <td>HBE023</td>
                        <td>DRY CLEANING</td>
                        <td>ONE MONTH</td>
                    </tr>
                       <tr>
                      
                        <td>24</td>
                        <td>HBE024</td>
                        <td>CAMPHOR MAKING</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>HBE025</td>
                        <td>PERFUME MAKING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>26</td>
                        <td>HBE026</td>
                        <td>MANUFACTURING OF JUTE BAGS, DOOR MATS, TABLE MATS</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>27</td>
                        <td>HBE027</td>
                        <td>REXIN BAG MAKING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>28</td>
                        <td>HBE028</td>
                        <td>LEATHER WORK</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>29</td>
                        <td>HBE029</td>
                        <td>BSS DIPLOMA IN BOOK BINDING</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>30</td>
                        <td>HBE030</td>
                        <td>CERTIFICATE IN BAMBOO CRAFTS ARTISANS</td>
                        <td>THREE MONTHS</td>
                    </tr>
                </tbody> 
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #11: HOME MAINTENANCE EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead> 
                 <tbody>
              <tr>
                        
                        <td>1</td>
                        <td>HME001</td>
                        <td>BSS DIPLOMA IN HOME SCIENCE</td>
                          <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>2</td>
                        <td>HME002</td>
                        <td>HOME MAINTENANCE TECHNICIAN</td>
                          <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                        
                        <td>3</td>
                        <td>HME003</td>
                        <td>BSS DIPLOMA IN DOMESTIC HOUSE KEEPING</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>HME004</td>
                        <td>DOMESTIC HELPER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>HME005</td>
                        <td>BSS DIPLOMA IN HOME NETWORK MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>HME006</td>
                        <td>HOME NETWORK ASSISTANT</td>
                        <td>ONE MONTH</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>HME007</td>
                        <td>BSS DIPLOMA IN HOME MANAGEMENT (MULTISKILLING AND RESIDENTIAL)</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>HME008</td>
                        <td>BSS DIPLOMA IN HOME MANAGEMENT AND GIREATRIC CARE,RESIDENTIAL & LI</td>
                        <td>TWO YEARS</td>
                    </tr>
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #12: INTERIOR & EXTERIOR EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead> 
                 <tbody>
              <tr>
                        
                        <td>1</td>
                        <td>IEE001</td>
                        <td>BSS DIPLOMA IN INTERIOR DESIGNING</td>
                          <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>2</td>
                        <td>IEE002</td>
                        <td>BSS DIPLOMA IN EXTERIOR DESIGNING</td>
                          <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>3</td>
                        <td>IEE003</td>
                        <td>BSS DIPLOMA IN INTERIOR & EXTERIOR DESIGNING</td>
                          <td>TWO YEARS</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>IEE004</td>
                        <td>EXHIBITION DESIGNER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>IEE005</td>
                        <td>INDOOR TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>IEE006</td>
                        <td>DESIGN PAINTER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>IEE007</td>
                        <td>SIGN BOARD WRITER</td>
                         <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>IEE008</td>
                        <td>THERMOCOL DECORATOR</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>IEE009</td>
                        <td>SHEET METAL FABRICATOR</td>
                       <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>IEE010</td>
                        <td>POT PAINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>11</td>
                        <td>IEE011</td>
                        <td>OIL PAINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>IEE012</td>
                        <td>GLASS PAINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>IEE013</td>
                        <td>CERAMIC PAINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>IEE014</td>
                        <td>DRAWING AND WATER COLOUR PAINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>IEE015</td>
                        <td>BLOCK PAINTING</td>
                        <td>THREE MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>IEE016</td>
                        <td>BSS DIPLOMA IN ALUMINUM FABRICATION</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>HBE017</td>
                        <td>BSS ADVANCED DIPLOMA IN INTERIOR DESIGNING</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>IEE018</td>
                        <td>BSS DIPLOMA IN FLORICULTURE & LAND SCAPPING TECHNOLOGY</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>19</td>
                        <td>IEE019</td>
                        <td>BSS DIPLOMA IN GARDEN SCIENCE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>IEE020</td>
                        <td>GARDEN SUPERVISOR</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>IEE021</td>
                        <td>LANDSCAPING TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>IEE022</td>
                        <td>BSS DIPLOMA IN INTERIOR DECORATION & DESIGNING</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>23</td>
                        <td>IEE023</td>
                        <td>ADVANCED DIPLOMA IN INTERIOR DESIGNING</td>
                        <td>TWO YEARS</td>
                    </tr>
                       <tr>
                      
                        <td>24</td>
                        <td>IEE024</td>
                        <td>BSS DIPLOMA IN INTERIOR ARCHITECTURE</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>25</td>
                        <td>IEE025</td>
                        <td>BSS DIPLOMA IN INTERIOR AND EXTERIOR ARCHITECTURE</td>
                        <td>TWO YEARS</td>
                    </tr>
                     <tr>
                      
                        <td>26</td>
                        <td>IEE026</td>
                        <td>BSS DIPLOMA IN INTERIOR AND EXTERIOR ARCHITECTURAL DESIGNING</td>
                        <td>TWO YEARS</td>
                    </tr>
                </tbody>
            </table>
                 <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #13: OFFICE MANAGEMENT EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead> 
                 <tbody>
              <tr>
                        
                        <td>1</td>
                        <td>OME001</td>
                        <td>BSS DIPLOMA IN OFFICE PRACTICE MANAGEMENT</td>
                          <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>2</td>
                        <td>OME002</td>
                        <td>BSS DIPLOMA IN SECRETARIAL PRACTICE</td>
                          <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>3</td>
                        <td>OME003</td>
                        <td>BSS DIPLOMA IN PERSONAL SECRETARYSHIP</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>OME004</td>
                        <td>OFFICE ASSISTANTSHIP</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>OME005</td>
                        <td>BSS DIPLOMA IN BASIC FINANCIAL SERVICES</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>OME006</td>
                        <td>PURCHASING & STORE KEEPING TECHNICIAN</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>OME007</td>
                        <td>RECEPTIONIST</td>
                         <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>OME008</td>
                        <td>BSS DIPLOMA IN OFFICE AUTOMATION</td>
                         <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>OME009</td>
                        <td>PUBLIC RELATIONS OFFICER</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>OME010</td>
                        <td>CLERK-CUM TYPIST</td>
                         <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>11</td>
                        <td>OME011</td>
                        <td>CASHIER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>OME012</td>
                        <td>OFFICE COMPUTER OPERATOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>OME013</td>
                        <td>BSS DIPLOMA IN OFFICE ACCOUNTING</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>OME014</td>
                        <td>CERTIFICATE IN ELECTRONIC OFFICE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>15</td>
                        <td>OME015</td>
                        <td>CERTIFICATE IN CLERICAL JOB IN COMPUTERISED OFFICE</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>16</td>
                        <td>OME016</td>
                        <td>CERTIFICATE IN COMPUTER IN RETAIL SHOP</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>17</td>
                        <td>OME017</td>
                        <td>BSS DIPLOMA IN COMPUTERISED SECRETARIAL MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>18</td>
                        <td>OME018</td>
                        <td>BSS DIPLOMA IN COMPUTERISED FINANCIAL MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>19</td>
                        <td>OME019</td>
                        <td>LEDGER CLERK</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>20</td>
                        <td>OME020</td>
                        <td>CERTIFICATE IN SECONDARY LEVEL TYPEWRITING TAMIL</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>21</td>
                        <td>OME021</td>
                        <td>CERTIFICATE IN SENIOR SECONDARY LEVEL TYPEWRITING ENGLISH</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>22</td>
                        <td>OME022</td>
                        <td>BSS DIPLOMA IN STENOGRAPHY</td>
                        <td>SIX MONTHS</td>
                    </tr>
                </tbody>
            </table>
             <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #14: POULTRY EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead> 
                 <tbody>
              <tr>
                        
                        <td>1</td>
                        <td>PE001</td>
                        <td>BSS DIPLOMA IN POULTRY TECHNOLOGY</td>
                          <td>TWO YEARS</td>
                    </tr>
                     <tr>
                        
                        <td>2</td>
                        <td>PE002</td>
                        <td>BSS DIPLOMA IN POULTRY PHARMACEUTICAL SALESMANSHIP</td>
                          <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>3</td>
                        <td>PE003</td>
                        <td>BSS DIPLOMA IN POULTRY HEALTH INSPECTOR</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>PE004</td>
                        <td>HATCHERY TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>PE005</td>
                        <td>CHICK SEXER</td>
                         <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>PE006</td>
                        <td>BSS DIPLOMA IN POULTRY FEED MANUFACTURING TECHNIQUES</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>PE007</td>
                        <td>POULTRY FEED TECHNICIAN</td>
                         <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>8</td>
                        <td>PE008</td>
                        <td>POULTRY LABORATORY TECHNICIAN</td>
                         <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>9</td>
                        <td>PE009</td>
                        <td>POULTRY PRODUCT TECHNICIAN</td>
                        <td>SIX MONTHS</td>
                    </tr>
                     <tr>
                      
                        <td>10</td>
                        <td>PE010</td>
                        <td>BSS DIPLOMA IN POULTRY FARM SUPERVISOR</td>
                         <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>11</td>
                        <td>PE011</td>
                        <td>POULTRY FARM RECORD KEEPER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>12</td>
                        <td>PE012</td>
                        <td>POULTRY FARMER</td>
                        <td>SIX MONTHS</td>
                    </tr>
                    <tr>
                      
                        <td>13</td>
                        <td>PE013</td>
                        <td>BSS DIPLOMA IN POULTRY EQUIPMENT MANUFACTURING TECHNIQUES</td>
                        <td>ONE YEAR</td>
                    </tr>
                     <tr>
                      
                        <td>14</td>
                        <td>PE014</td>
                        <td>CERTIFICATE COURSE IN POULTRY MANAGEMENT</td>
                        <td>ONE YEAR</td>
                    </tr>
                </tbody>
            </table>
            <hr class="space m" />
             <h3 class=" text-center text-color">CATEGORY COURSES #14: POULTRY EDUCATION</h3>
            <hr class="space m" />
            <table class="table">
                <thead>
                   <tr>
                       <th>SNO</th>
                       <th>CODE</th>
                        <th> COURSES </th>
                        <th>DURATION</th>
                    </tr>
                </thead> 
                 <tbody>
              <tr>
                        
                        <td>1</td>
                        <td>VE001</td>
                        <td>BSS DIPLOMA IN VETERINARY SCIENCES</td>
                          <td>TWO YEARS</td>
                    </tr>
                     <tr>
                        
                        <td>2</td>
                        <td>VE002</td>
                        <td>LIVESTOCK TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                     <tr>
                        
                        <td>3</td>
                        <td>VE003</td>
                        <td>VETERINARY LABORATORY TECHNICIAN</td>
                          <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>4</td>
                        <td>VE004</td>
                        <td>LIVESTOCK DAIRY SUPERVISOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>5</td>
                        <td>VE005</td>
                        <td>BSS DIPLOMA IN VETERINARY PHARMACY</td>
                         <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>6</td>
                        <td>VE006</td>
                        <td>BSS DIPLOMA IN VETERINARY INSPECTOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                    <tr>
                      
                        <td>7</td>
                        <td>VE007</td>
                        <td>BSS DIPLOMA IN LIVESTOCK INSPECTOR</td>
                        <td>ONE YEAR</td>
                    </tr>
                    </tbody>
                </table>
          </div>
    </div>

 
 