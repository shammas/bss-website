
      <div class="section-empty no-paddings">
        <div class="section-slider row-18 white">
            <div class="flexslider advanced-slider slider visible-dir-nav" data-options="animation:fade">
                <ul class="slides">
                    <li data-slider-anima="fade-left" data-time="1000">
                        <div class="section-slide">
                    <div class="bg-cover" style="background-image:url('<?php echo base_url();?>assets/images/SKM_6534.JPG')">
                            </div>
                         
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('<?php echo base_url();?>assets/images/_KM_4655.JPG')">                        
                            </div>                           
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('<?php echo base_url();?>assets/images/31.jpg')">                        
                            </div>                           
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="home_page">
        <div class="home-about">
<div class="section-empty">
        <div class="container content">
            <div class="row vertical-row">
                <div class="col-md-6 col-sm-12">
                    <h1 class="text-color">KNOW ABOUT BHARAT SEVAK SAMAJ</h1>
                    <h4></h4>
                    <p>
                      The Bharat Sevak Samaj was promoted by the Planning Commission, Government of India, in the background first Five Year Plan in 1952 to provide national platform for constructive work, after the achievement of Independence. Father of the nation Mahatma Gandhi visualized the formation of an organization named as 'Lok Sevak Sangh' to take up the programmed of socio-economic reconstruction of the country. The idea was further pursed by two of his greatest stalwarts Prime Minister Pt. Jawaharlal Nehru and Shri Gulzarilal Nanda, then Minister of planning with the launching of the planned economic development of nation. The National Advisory Committee of the Planning Commission for public Co-operation concretized the idea after holding consultations with prominent public leaders and lining and leading politicians of all parties. Smt. Indira Gandhi, Shri Shahnawaz Khan one of the closest associates of Netaji Subhash Chandra Bose, Prof. N.R Malkani renowned Gandhian and few other eminent men were the founder members of Bharat Sevak Samaj under whose signatures its constitutions was registered and the Bharat Sevak Samaj was finally launched on 12th August 1952. 
                    </p>
                    <hr class="space s" />
                    <a href="about-us" target="_blank" class="btn btn-sm">About BSS</a>
                      <hr class="space" />  
                </div>
                <div class="col-md-6 col-sm-12">
                    <img class="shadow-2" src="<?php echo base_url();?>assets/images/About BSS-2.jpg" alt="" />
                </div>
            </div>            
        </div>
    </div>
</div>
   <div class="section-bg-color">
        <div class="container content">
            <div class="row proporzional-row">
                <div class="col-md-3 ">
                    <img class="shadow-2" src="<?php echo base_url();?>assets/images/news11.jpg" alt="" />
                </div>
                <hr class="space s" />
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-12 boxed-inverse boxed-border white">
                             <h2 class="text-color"> Bharat Sevak Samaj Action Plan Meet held at chennai on May 1,2008</h2>
                            <p> 
                                Source:The Hindu ,May 2,2008
                            </p>
                        </div>
                    </div>
                </div>   
                <hr class="space s" />             
                <div class="col-md-3 ">
                    <img class="shadow-2" src="<?php echo base_url();?>assets/images/news21.jpg" alt="" />
                </div>
                <hr class="space s" />
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-12 boxed-inverse boxed-border white">
                            <h2 class="text-color">Bharat Sevak Samaj officials interview with media on Aug 08,2007</h2>
                            <p> Source:The Hindu ,Aug 09,2007
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="five-points">
    <div class="section-empty">
        <div class="container content">
            <h1 class="text-color">FIVE-POINT CODE FOR  BSS WORKERS</h1>
             <hr class="space s" />
              <div class="row ">
                <div class="col-md-12 col-xs-12">
                    <div class="flexslider carousel outer-navs" data-options="numItems:3,itemMargin:15,controlNav:true,directionNav:true">
                        <ul class="slides">
                            <li>
                                <div class="advs-box advs-box-top-icon boxed-inverse" data-anima="scale-up" data-trigger="hover">
                                    <img width="120" src="<?php echo base_url();?>assets/images/logo.png" alt="logo" />
                                    <hr class="space s" />
                                    <h3>Integrity</h3>
                                    <p>
                                        Workers should have the character quality of being honest, excellent moral character, reliable and fair.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="advs-box advs-box-top-icon boxed-inverse" data-anima="scale-up" data-trigger="hover">
                                      <img width="120" src="<?php echo base_url();?>assets/images/logo.png" alt="logo" />
                                     <hr class="space s" />
                                    <h3>Loyalty to the Nation</h3>
                                    <p>
                                        Workers should have loyalty to the nation and obey the rules and regulations of the nation firmly for the integrity and peacefulness of the nation.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="advs-box advs-box-top-icon boxed-inverse" data-anima="scale-up" data-trigger="hover">
                                     <img width="120" src="<?php echo base_url();?>assets/images/logo.png" alt="logo" />
                                     <hr class="space s" />
                                    <h3>Selfless Service</h3>
                                    <p>
                                        Workers should be ready to serve the nation and the society without any expectation of personal benefits or award.
                                    </p>
                                </div>
                            </li>
                             <li>
                                <div class="advs-box advs-box-top-icon boxed-inverse" data-anima="scale-up" data-trigger="hover">
                                     <img width="120" src="<?php echo base_url();?>assets/images/logo.png" alt="logo" />
                                    <hr class="space s" />
                                    <h3>Peaceful Conduct</h3>
                                    <p>
                                        Workers should have an attitude of maintaining tranquility and honoring the dignity of others. They must not participate in any act that constitutes violent behavior.
                                    </p>
                                </div>
                            </li>
                             <li>
                                <div class="advs-box advs-box-top-icon boxed-inverse" data-anima="scale-up" data-trigger="hover">
                                     <img width="120" src="<?php echo base_url();?>assets/images/logo.png" alt="logo" />
                                    <hr class="space s" /> 
                                    <h3>Economic Living</h3>
                                    <p>
                                        Workers should have strict financial discipline, moderate spending behavior and they should not spend resources lavishly without any control.
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
              
            </div>
</div>
</div>
</div>
 