<?php
/**
 * Center_Controller.php
 * Date: 18/11/2020
 * Time: 3:13 PM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Center_Controller extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Center_model', 'center');
            $this->load->library(['ion_auth']);
        $this->load->library('form_validation');
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('login'));
        }
    }

    function index()
    {
        $data = $this->center->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    function get_all()
    {
        $data = $this->center->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {

        $this->form_validation->set_rules('name', 'Center', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            if ($this->center->insert($post_data)) {
                $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
            }
        }
    }

    function update($id){
        $this->form_validation->set_rules('name', 'Center', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            if($this->center->update($post_data,$id)) {
                $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
            }else {
                $this->output->set_status_header(500, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['validation_error' => 'Please select images.']));
            }
        }
    }


    public function delete($id)
    {
        $center = $this->center->with_file()->where('id', $id)->get();
        if ($center) {
            $this->center->delete($id);
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Center Deleted']));
        } else {
            $this->output->set_status_header(500, 'Validation error');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }


}