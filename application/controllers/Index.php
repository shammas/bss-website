<?php
/**
 * Index.php
 * Date: 08/10/2020
 * Time: 03:03 PM
 */

class Index extends CI_Controller { 
	 
    protected $header = 'templates/header';
    protected $footer = 'templates/footer';

    public function __construct()
    {
        parent::__construct();

        $this->load->library('email');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('javascript');

        $this->load->model('Gallery_model', 'gallery');
        $this->load->model('Center_model', 'center');
    }

    protected $current = '';

    public function index()
    {
        $this->current = 'index';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('index');
        $this->load->view($this->footer);
    }

    public function about_bss()
    {
        // $this->current = 'about-bss';
        // $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('about-bss');
        // $this->load->view($this->footer);
    }

    public function about_us()
    {
        $this->current = 'about-us';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('about-us');
        $this->load->view($this->footer);
    }
    public function approved_centers()
    {
        $data['centers'] = $this->center->order_by('id','desc')->get_all();

        $this->current = 'approved-centers';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('approved-centers',$data);
        $this->load->view($this->footer);
    }
    public function become_a_bss_centre()
    {
        $this->current = 'become-a-bss-centre';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('become-a-bss-centre');
        $this->load->view($this->footer);
    }
    public function become_an_associate()
    {
        $this->current = 'become-an-associate';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('become-an-associate');
        $this->load->view($this->footer);
    }
    public function category_courses()
    {
        $this->current = 'category-courses';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('category-courses');
        $this->load->view($this->footer);
    }
    public function contact_us()
    {
        $this->current = 'contact-us';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('contact-us');
        $this->load->view($this->footer);
    }
    public function courses()
    {
        $this->current = 'courses';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('courses');
        $this->load->view($this->footer);
    }
    public function gallery()
    {
        $data['galleries'] = $this->gallery->order_by('id','desc')->get_all();

        $this->current = 'gallery';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('gallery',$data);
        $this->load->view($this->footer);
    }
    public function it_courses()
    {
        $this->current = 'it-courses';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('it-courses');
        $this->load->view($this->footer);
    }
    public function medical_technology_courses()
    {
        $this->current = 'medical-technology-courses';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('medical-technology-courses');
        $this->load->view($this->footer);
    }
    public function my_skill_certificate()
    {
        $this->current = 'my-skill-certificate';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('my-skill-certificate');
        $this->load->view($this->footer);
    }
    public function online_courses()
    {
        $this->current = 'online-courses';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('online-courses');
        $this->load->view($this->footer);
    }
    public function school_courses()
    {
        $this->current = 'school-courses';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('school-courses');
        $this->load->view($this->footer);
    }
    public function special_courses()
    {
        $this->current = 'special-courses';
        $this->load->view($this->header, ['current' => $this->current]);
        $this->load->view('special-courses');
        $this->load->view($this->footer);
    }
    
    
    public function contact_us_mail(){

        $this->load->library('email');
          
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $mobilno = $this->input->post('mobilno');
        $whatsappno = $this->input->post('whatsappno');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');
         $config = [           
          'protocol' => 'smtp',           
            'smtp_host' => 'ssl://mail.bssmalappuram.org',            
            'smtp_port' => 465,            
            'smtp_user' => 'no-reply@bssmalappuram.org',            
            'smtp_pass' => '$Ob$H=y6}(k+',            
            'mailtype' => 'html',            
            'charset' => 'iso-8859-1',            
            'wordwrap' => TRUE        
        ];
        $this->email->initialize($config);
        
        $message = wordwrap($message, 70, "\n");

        $content  = '<b>Name    :</b>   ' . $name . '<br>';
        $content .= '<b>Email   :</b> ' . $email . '<br>' ;
        $content .= '<b>Mobile No.    :</b>   ' . $mobilno . '<br>';
        $content .= '<b>WhatsApp No.    :</b>   ' . $whatsappno . '<br>';
        $content .= '<b>Subject   :</b>  ' . $subject . '<br>';
        $content .= 'Message  :</b>   ' . $message .'<br>';

        $this->email->set_newline("\r\n");
        $this->email->from('no-reply@bssmalappuram.org ');
        $this->email->to('bssmalappuram@gmail.com');
        $this->email->subject('Contact Us Message');

        $this->email->message($content);

        if ($this->email->send()) {
            echo "<script>
            alert('Message Sent Successfully..!!');
            window.location.href='contact-us';
            </script>";
        }
        else {
            $this->output->set_status_header(400, 'Unable to send mail');
            $this->output->set_output("Please try again later.");
        }
    }

    public function online_courses_application(){

        $this->load->library('email');
          
        $name = $this->input->post('name');
        $age = $this->input->post('age');
        $dob = $this->input->post('dob');
        $sex = $this->input->post('sex');
        $place = $this->input->post('place');
        $education = $this->input->post('education');
        $mobileno = $this->input->post('mobileno');
        $whatsappno = $this->input->post('whatsappno');
        $email = $this->input->post('email');
        $course = $this->input->post('course');
        $message = $this->input->post('message');
         $config = [           
          'protocol' => 'smtp',           
            'smtp_host' => 'ssl://mail.bssmalappuram.org',            
            'smtp_port' => 465,            
            'smtp_user' => 'no-reply@bssmalappuram.org',            
            'smtp_pass' => '$Ob$H=y6}(k+',           
            'mailtype' => 'html',            
            'charset' => 'iso-8859-1',            
            'wordwrap' => TRUE        
        ];
        $this->email->initialize($config);
        
        $message = wordwrap($message, 70, "\n");

        $content  = '<b>Name    :</b>   ' . $name . '<br>';
        $content .= '<b>Age    :</b>   ' . $age . '<br>';
        $content .= '<b>Date of Birth    :</b>   ' . $dob . '<br>';
        $content .= '<b>Sex    :</b>   ' . $sex . '<br>';
        $content .= '<b>Place    :</b>   ' . $place . '<br>';
        $content .= '<b>Education    :</b>   ' . $education . '<br>';
        $content .= '<b>Mobile No.    :</b>   ' . $mobileno . '<br>';
        $content .= '<b>WhatsApp No.    :</b>   ' . $whatsappno . '<br>';
        $content .= '<b>Email   :</b> ' . $email . '<br>' ;
        $content .= '<b>Course    :</b>   ' . $course . '<br>';
        $content .= '<b>Message  :</b>   ' . $message ;

        $this->email->set_newline("\r\n");
        $this->email->from('no-reply@bssmalappuram.org');
        $this->email->to('bssmalappuram@gmail.com');
        $this->email->subject('Application for Online Courses');

        $this->email->message($content);

        if ($this->email->send()) {
            echo "<script>
                alert('Message Sent Successfully..!!');
                window.location.href='online-courses';
            </script>";
        }
        else {
            $this->output->set_status_header(400, 'Unable to send mail');
            $this->output->set_output("Please try again later.");
        }
    }

    public function centre_affiliation_application(){

        $this->load->library('email');
          
        $name = $this->input->post('name');
        $age = $this->input->post('age');
        $sex = $this->input->post('sex');
        $place = $this->input->post('place');
        $education = $this->input->post('education');
        $mobileno = $this->input->post('mobileno');
        $whatsappno = $this->input->post('whatsappno');
        $email = $this->input->post('email');
        $course = $this->input->post('course');
        $message = $this->input->post('message');
         $config = [           
          'protocol' => 'smtp',           
            'smtp_host' => 'ssl://mail.bssmalappuram.org',            
            'smtp_port' => 465,            
            'smtp_user' => 'no-reply@bssmalappuram.org',            
            'smtp_pass' => '$Ob$H=y6}(k+',           
            'mailtype' => 'html',            
            'charset' => 'iso-8859-1',            
            'wordwrap' => TRUE        
        ];
        $this->email->initialize($config);
        
        $message = wordwrap($message, 70, "\n");

        $content  = '<b>Name    :</b>   ' . $name . '<br>';
        $content .= '<b>Age    :</b>   ' . $age . '<br>';
        $content .= '<b>Sex    :</b>   ' . $sex . '<br>';
        $content .= '<b>Place    :</b>   ' . $place . '<br>';
        $content .= '<b>Education    :</b>   ' . $education . '<br>';
        $content .= '<b>Mobile No.    :</b>   ' . $mobileno . '<br>';
        $content .= '<b>WhatsApp No.    :</b>   ' . $whatsappno . '<br>';
        $content .= '<b>Email   :</b> ' . $email . '<br>' ;
        $content .= '<b>Courses    :</b>   ' . $course . '<br>';
        $content .= '<b>Message  :</b>   ' . $message ;

        $this->email->set_newline("\r\n");
        $this->email->from('no-reply@bssmalappuram.org');
        $this->email->to('bssmalappuram@gmail.com');
        $this->email->subject('Application for Centre Affiliation');

        $this->email->message($content);

        if ($this->email->send()) {
            echo "<script>
                alert('Message Sent Successfully..!!');
                window.location.href='become-a-bss-centre';
            </script>";
        }
        else {
            $this->output->set_status_header(400, 'Unable to send mail');
            $this->output->set_output("Please try again later.");
        }
    }
    public function become_associate_application(){

        $this->load->library('email');
          
        $name = $this->input->post('name');
        $age = $this->input->post('age');
        $sex = $this->input->post('sex');
        $mobileno = $this->input->post('mobileno');
        $whatsappno = $this->input->post('whatsappno');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $education = $this->input->post('education');
        $course = $this->input->post('course');
        $message = $this->input->post('message');
         $config = [           
          'protocol' => 'smtp',           
            'smtp_host' => 'ssl://mail.bssmalappuram.org',            
            'smtp_port' => 465,            
            'smtp_user' => 'no-reply@bssmalappuram.org',            
            'smtp_pass' => '$Ob$H=y6}(k+',           
            'mailtype' => 'html',            
            'charset' => 'iso-8859-1',            
            'wordwrap' => TRUE        
        ];
        $this->email->initialize($config);
        
        $message = wordwrap($message, 70, "\n");

        $content  = '<b>Name    :</b>   ' . $name . '<br>';
        $content .= '<b>Age    :</b>   ' . $age . '<br>';
        $content .= '<b>Sex    :</b>   ' . $sex . '<br>';
        $content .= '<b>Mobile No.    :</b>   ' . $mobileno . '<br>';
        $content .= '<b>WhatsApp No.    :</b>   ' . $whatsappno . '<br>';
        $content .= '<b>Email   :</b> ' . $email . '<br>' ;
        $content .= '<b>Address    :</b>   ' . $address . '<br>';
        $content .= '<b>Education    :</b>   ' . $education . '<br>';
        $content .= '<b>Coaching Field    :</b>   ' . $course . '<br>';
        $content .= '<b>Message  :</b>   ' . $message ;

        $this->email->set_newline("\r\n");
        $this->email->from('no-reply@bssmalappuram.org');
        $this->email->to('bssmalappuram@gmail.com');
        $this->email->subject('Application for associate');

        $this->email->message($content);

        if ($this->email->send()) {
            echo "<script>
                alert('Message Sent Successfully..!!');
                window.location.href='become-an-associate';
            </script>";
        }
        else {
            $this->output->set_status_header(400, 'Unable to send mail');
            $this->output->set_output("Please try again later.");
        }
    }

    public function skill_certificate_application(){

        $this->load->library('email');
          
        $name = $this->input->post('name');
        $age = $this->input->post('age');
        $sex = $this->input->post('sex');
        $mobileno = $this->input->post('mobileno');
        $whatsappno = $this->input->post('whatsappno');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $education = $this->input->post('education');
        $course = $this->input->post('course');
        $message = $this->input->post('message');
         $config = [           
          'protocol' => 'smtp',           
            'smtp_host' => 'ssl://mail.bssmalappuram.org',            
            'smtp_port' => 465,            
            'smtp_user' => 'no-reply@bssmalappuram.org',            
            'smtp_pass' => '$Ob$H=y6}(k+',           
            'mailtype' => 'html',            
            'charset' => 'iso-8859-1',            
            'wordwrap' => TRUE        
        ];
        $this->email->initialize($config);
        
        $message = wordwrap($message, 70, "\n");

        $content  = '<b>Name    :</b>   ' . $name . '<br>';
        $content .= '<b>Age    :</b>   ' . $age . '<br>';
        $content .= '<b>Sex    :</b>   ' . $sex . '<br>';
        $content .= '<b>Mobile No.    :</b>   ' . $mobileno . '<br>';
        $content .= '<b>WhatsApp No.    :</b>   ' . $whatsappno . '<br>';
        $content .= '<b>Email   :</b> ' . $email . '<br>' ;
        $content .= '<b>Address    :</b>   ' . $address . '<br>';
        $content .= '<b>Education    :</b>   ' . $education . '<br>';
        $content .= '<b>Name of the trade    :</b>   ' . $course . '<br>';
        $content .= '<b>Message  :</b>   ' . $message ;

        $this->email->set_newline("\r\n");
        $this->email->from('no-reply@bssmalappuram.org');
        $this->email->to('bssmalappuram@gmail.com');
        $this->email->subject('Application for Skill Certificate');

        $this->email->message($content);

        if ($this->email->send()) {
            echo "<script>
                alert('Message Sent Successfully..!!');
                window.location.href='my-skill-certificate';
            </script>";
        }
        else {
            $this->output->set_status_header(400, 'Unable to send mail');
            $this->output->set_output("Please try again later.");
        }
    }
}