<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*_______ public route start _______*/
$route['index'] = 'Index/index';
$route['about-bss'] = 'Index/about_bss';
$route['about-us'] = 'Index/about_us';
$route['approved-centers'] = 'Index/approved_centers';
$route['become-a-bss-centre'] = 'Index/become_a_bss_centre';
$route['become-an-associate'] = 'Index/become_an_associate';
$route['category-courses'] = 'Index/category_courses';
$route['contact-us'] = 'Index/contact_us';
$route['courses'] = 'Index/courses';
$route['gallery'] = 'Index/gallery';
$route['it-courses'] = 'Index/it_courses';
$route['medical-technology-courses'] = 'Index/';
$route['medical-technology-courses'] = 'Index/medical_technology_courses';
$route['my-skill-certificate'] = 'Index/my_skill_certificate';
$route['online-courses'] = 'Index/online_courses';
$route['school-courses'] = 'Index/school_courses';
$route['special-courses'] = 'Index/special_courses';



/****Dashboard***/
$route['login'] = 'Auth/login';
$route['logout'] = 'Auth/logout';
$route['change_password'] = 'Auth/change_password';

$route['dashboard/load-user'] = 'Dashboard/load_user';
$route['dashboard/edit-user/(:num)']['POST'] = 'Auth/edit_user/$1';

$route['dashboard'] = 'Dashboard';
$route['dashboard/(:any)'] = 'Dashboard/page/$1';

$route['dashboard/gallery/get'] = 'Gallery_Controller';
$route['dashboard/gallery/upload'] = 'Gallery_Controller/upload';
$route['dashboard/gallery/add']['post'] = 'Gallery_Controller/store';
$route['dashboard/gallery/edit/(:num)']['post'] = 'Gallery_Controller/update/$1';
$route['dashboard/gallery/delete/(:num)']['delete'] = 'Gallery_Controller/delete/$1';

$route['dashboard/center/get'] = 'Center_Controller/index';
$route['dashboard/center/add']['post'] = 'Center_Controller/store';
$route['dashboard/center/edit/(:num)']['post'] = 'Center_Controller/update/$1';
$route['dashboard/center/delete/(:num)']['delete'] = 'Center_Controller/delete/$1';

/*** Send Mail ***/
$route['contact_us_mail'] = 'Index/contact_us_mail';
$route['online_courses_application'] = 'Index/online_courses_application';
$route['centre_affiliation_application'] = 'Index/centre_affiliation_application';
$route['become_associate_application'] = 'Index/become_associate_application';
$route['skill_certificate_application'] = 'Index/skill_certificate_application';