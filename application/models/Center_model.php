<?php
/**
 * Center_model.php
 * Date: 18/11/20
 * Time: 03:27 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Center_model extends MY_Model
{
 public $table = 'centers';
    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}