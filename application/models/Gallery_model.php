<?php
/**
 * Gallery_model.php
 * Date: 08/11/19
 * Time: 03:47 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Gallery_model extends MY_Model
{
 public $table = 'galleries';
    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}